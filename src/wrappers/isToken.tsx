import { Redirect } from 'umi';
import { userLocalStore } from '@/pages/login/Login';

export default (props) => {
  try {
    const userInfo = JSON.parse(window.localStorage.getItem('userInfo'));
    const expires_in = userInfo?.expires_in;
    if (expires_in > Date.now()) {
      return props.children;
    }
    return <Redirect to="/login"></Redirect>;
  } catch (error) {
    return <Redirect to="/login"></Redirect>;
  }
};
