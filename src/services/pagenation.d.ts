export interface PageParams {
  current: number;
  size: number;
}

export interface LimitParams {
  page: number;
  limit: number;
  beanName: string;
}
