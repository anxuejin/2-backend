import { request } from 'umi';
import { PageParams } from './shops';
export const getNoticeList = (params: PageParams) =>
  request('/api/shop/notice/page', {
    method: 'get',
    params,
  });

export const getSearchList = (params: PageParams) =>
  request('/api/admin/hotSearch/page', {
    method: 'get',
    params,
  });

export const getAdminList = (params: PageParams) =>
  request('/api/admin/indexImg/page', {
    method: 'get',
    params,
  });

export const getTransportList = (params: PageParams) =>
  request('/api/shop/transport/page', {
    method: 'get',
    params,
  });

export const getPickaddrtList = (params: PageParams) =>
  request('/api/shop/pickAddr/page', {
    method: 'get',
    params,
  });

export const deladmin = (data: any) =>
  request('/api/admin/indexImg', {
    method: 'delete',
    data,
  });

export const delSearch = (data: any) =>
  request('/api/admin/hotSearch', {
    method: 'delete',
    data,
  });

export const delNotice = (data: any) =>
  request(`/api/shop/notice/${data}`, {
    method: 'delete',
  });

export const delPickaddr = (data: any) =>
  request(`/api/shop/pickAddr`, {
    method: 'delete',
    data,
  });

export const delTransport = (data: any) =>
  request(`/api/shop/transport`, {
    method: 'delete',
    data,
  });

export const delcheck = (data: any) =>
  request(`/api/admin/hotSearch`, {
    method: 'delete',
    data,
  });

export const delAdmin = (data: any) =>
  request(`/api/admin/indexImg`, {
    method: 'delete',
    data,
  });

export const delPickAddr = (data: any) =>
  request(`/api/shop/pickAddr`, {
    method: 'delete',
    data,
  });

export const delTransPort = (data: any) =>
  request(`/api/shop/transport`, {
    method: 'delete',
    data,
  });
//admin/hotSearch

export const addHotSearch = (data: any) =>
  request(`/api/admin/hotSearch`, {
    method: 'post',
    data,
  });

export const getProdTagData = (data: any) =>
  request(`/api/admin/hotSearch/info/${data}`, {
    method: 'get',
  });

export const putHotSearch = (data: any) =>
  request(`/api/admin/hotSearch`, {
    method: 'put',
    data,
  });

export const addNotice = (data: any) =>
  request(`/api/shop/notice`, {
    method: 'post',
    data,
  });

export const getNotice = (data: any) =>
  request(`/api/shop/notice/info/${data}`, {
    method: 'get',
  });

export const putNotice = (data: any) =>
  request(`/api/shop/notice`, {
    method: 'put',
    data,
  });
