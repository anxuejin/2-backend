import { request } from 'umi';

export const login = (data: any) =>
  request('/api/login', {
    method: 'post',
    data,
  });

export const getNav = () => request('/api/sys/menu/nav', {});
