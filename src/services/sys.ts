// 系统管理
import { request } from 'umi';
import { PageParams, LimitParams } from './pagenation.d';
// 管理员列表
// 数据获取
export const getUserList = (params: PageParams) =>
  request('/api/sys/user/page', {
    method: 'get',
    params,
  });
// 角色信息数据获取
export const getRoleList = () =>
  request('/api/sys/role/list', {
    method: 'get',
  });

// 编辑回显的数据
export const getUserInfo = (id: number) =>
  request(`/api/sys/user/info/${id}`, {
    method: 'get',
  });

// 编辑
export const editUser = (data: any) =>
  request('/api/sys/user', {
    method: 'put',
    data,
  });

// 新增
export const addUser = (data: any) =>
  request('/api/sys/user', {
    method: 'post',
    data,
  });

// 管理员列表单个/批量删除
export const delUser = (data: any) =>
  request('/api/sys/user', {
    method: 'delete',
    data,
  });

// 地址管理
// 数据获取
export const getAreaList = () => request('/api/admin/area/list');
export const delAreaItem = (id: number) =>
  request('/api/admin/area/' + id, {
    method: 'DELETE',
  });
export interface AreaItemParams {
  areaId: number;
  areaName: string;
  areas: null;
  level: number;
  parentId: number;
}

export const areaItem = (method: 'post' | 'put', data: AreaItemParams) =>
  request('/api/admin/area', {
    method,
    data,
  });

// 角色管理
// 数据获取
export const getRole = (params: PageParams) =>
  request('/api/sys/role/page', {
    method: 'get',
    params,
  });
// 角色管理单个/批量删除
export const delRole = (data: any) =>
  request('/api/sys/role', {
    method: 'delete',
    data,
  });
// 授权数据/菜单管理数据
export const getMenu = () =>
  request('/api/sys/menu/table', {
    method: 'get',
  });
// 角色编辑回显的数据
export const getRoleInfo = (id: number) =>
  request(`/api/sys/role/info/${id}`, {
    method: 'get',
  });
// 角色新增
export const addRole = (data: any) =>
  request('/api/sys/role', {
    method: 'post',
    data,
  });
// 角色编辑
export const editRole = (data: any) =>
  request('/api/sys/role', {
    method: 'put',
    data,
  });

// 定时任务
// 表格数据获取/搜索
export const getSchedule = (params: LimitParams) =>
  request('/api/sys/schedule/page', {
    method: 'get',
    params,
  });
// 定时任务单个/批量删除
export const delSchedule = (data: any) =>
  request('/api/sys/schedule', {
    method: 'delete',
    data,
  });
// 定时任务单个/批量暂停
export const stopSchedule = (data: any) =>
  request('/api/sys/schedule/pause', {
    method: 'post',
    data,
  });
// 定时任务单个/批量恢复
export const recoverySchedule = (data: any) =>
  request('/api/sys/schedule/resume', {
    method: 'post',
    data,
  });
// 定时任务单个/批量立即执行
export const impSchedule = (data: any) =>
  request('/api/sys/schedule/run', {
    method: 'post',
    data,
  });
// 定时任务修改回显的数据
export const getScheduleInfo = (id: number) =>
  request(`/api/sys/schedule/info/${id}`, {
    method: 'get',
  });
// 定时任务修改
export const editSchedule = (data: any) =>
  request('/api/sys/schedule', {
    method: 'put',
    data,
  });
// 定时任务增加
export const addSchedule = (data: any) =>
  request('/api/sys/schedule', {
    method: 'post',
    data,
  });
