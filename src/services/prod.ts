import { request } from 'umi';
import { PageParams } from './pagenation';
//产品管理数据
export const getProdList = (params: PageParams) =>
  request('/api/prod/prod/page', {
    method: 'get',
    params,
  });
//分组管理数据
export const getProdTag = (params: PageParams) =>
  request('/api/prod/prodTag/page', {
    method: 'get',
    params,
  });
//分组管理新增接口
export const getProdTagAdd = (data: any) =>
  request('/api/prod/prodTag', {
    method: 'post',
    data,
  });
//分组管理编辑回显接口
export const getProdTagData = (data: any) =>
  request(`/api/prod/prodTag/info/${data}`, {
    method: 'get',
  });
//分组管理删除接口
export const getProdTagDel = (data: any) =>
  request(`/api/prod/prodTag/${data}`, {
    method: 'delete',
  });
//分组管理编辑接口
export const getProdTagEdit = (data: any) =>
  request(`/api/prod/prodTag`, {
    method: 'put',
    data,
  });
//分类管理数据
export const getProdCategory = (params: PageParams) =>
  request('/api/prod/category/table', {
    method: 'get',
    params,
  });
//分类管理新增接口
export const addClassifyList = (data: any) =>
  request('/api/prod/prodTag', {
    method: 'post',
    data,
  });
//分类管理删除接口
export const deleteClassifyList = (data: any) =>
  request(`/api/prod/prodTag/${data}`, {
    method: 'delete',
  });
//分类管理编辑接口
export const editClassifyList = (data: any) =>
  request(`/api/prod/prodTag`, {
    method: 'put',
    data,
  });
//分类管理编辑回显接口
export const showClassifyList = (data: any) =>
  request(`/api/prod/category/info/${data}`, {
    method: 'get',
  });
//评论管理数据
export const getProdComm = (params: PageParams) =>
  request('/api/prod/prodComm/page', {
    method: 'get',
    params,
  });
//规格管理数据
export const getProdSpec = (params: PageParams) =>
  request('/api/prod/spec/page', {
    method: 'get',
    params,
  });
//规格管理新增接口
export const getProdSpecAdd = (data: any) =>
  request('/api/prod/spec', {
    method: 'post',
    data,
  });
//规格管理删除接口
export const getProdSpecDel = (id: number) =>
  request(`/api/prod/spec/${id}`, {
    method: 'delete',
  });
//规格管理编辑
export const Editnorms = (data: any) =>
  request('/api/prod/spec', {
    method: 'put',
    data,
  });
// 系统日志数据
export const getSyslog = (params: PageParams) =>
  request('/api/sys/log/page', {
    method: 'get',
    params,
  });
// 订单管理数据
export const getOrderList = (params: PageParams) =>
  request('/api/order/order/page', {
    method: 'get',
    params,
  });
//会员管理数据
export const getList = (params: PageParams) =>
  request('/api/admin/user/page', {
    method: 'get',
    params,
  });
//参数管理数据
export const getCan = (params: PageParams) =>
  request('/api/sys/config/page', {
    method: 'get',
    params,
  });
//参数管理数据增加接口
export const getZen = (data: PageParams) =>
  request('/api/sys/config', {
    method: 'post',
    data,
  });
//参数管理删除接口
export const getShan = (data: any) =>
  request(`/api/sys/config`, {
    method: 'delete',
    data,
  });
//获取产品分类
export const getCategoryList = () =>
  request('/api/prod/category/listCategory', {
    method: 'get',
  });

//获取产品分组
export const getTagList = () =>
  request('/api/prod/prodTag/listTagList', {
    method: 'get',
  });

//运费设置
export const getTransportList = () =>
  request('/api/shop/transport/list', {
    method: 'get',
  });
//根据id获取运费模板
export const getTransportId = (id: number | string) =>
  request('/api/shop/transport/info/' + id, {
    method: 'get',
  });
