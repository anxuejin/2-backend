import React, { useState } from 'react';
import type { ProColumns } from '@ant-design/pro-components';
import { ProTable } from '@ant-design/pro-components';
import { Button, Tag, Image, Select } from 'antd';
import { getList } from '@/services/prod';
import {
  EditOutlined,
  DeleteOutlined,
  PlusOutlined,
  RedoOutlined,
  AppstoreOutlined,
  SearchOutlined,
} from '@ant-design/icons';
import classNames from 'classnames';
import styles from '../../prod/style.less';
import ColumnsTransfer from '@/components/columnsTransfer';
enum Status {
  禁用,
  正常,
}
interface ProdTableColums {
  title: string;
  dataIndex?: string;
}
const columns: ProColumns<ProdTableColums>[] = [
  {
    title: '用户昵称',
    dataIndex: 'nickName',
    width: 200,
    align: 'center',
  },
  {
    title: '用户头像',
    align: 'center',
    render(text, record, index) {
      return (
        <Image
          width={200}
          src={record?.pic}
          fallback="https://ts1.cn.mm.bing.net/th/id/R-C.d5db82cbbec25fbe9bd559cc82d586fd?rik=kKzks2RY8lCSPg&riu=http%3a%2f%2fbpic.588ku.com%2felement_list_pic%2f19%2f04%2f22%2f53b40a019c5a84ac1aca2a85eea44889.jpg&ehk=4NLlZ%2fhJhYpAhmr8kNXCuCXijJTIkVz6QVq0GsKZPbI%3d&risl=&pid=ImgRaw&r=0&sres=1&sresct=1"
        />
      );
    },
    search: false,
  },
  {
    title: '状态',
    align: 'center',
    dataIndex: 'status',
    render(text, record, index) {
      return <Tag color="processing">{Status[record.status]}</Tag>;
    },
    renderFormItem(item, options, form) {
      const handleStatusChange = (value: number) => {
        form.setFieldValue('status', value);
      };
      return (
        <Select placeholder={'请选择状态'} onChange={handleStatusChange}>
          <Select.Option value={Status['禁用']}>{Status[0]}</Select.Option>
          <Select.Option value={Status['正常']}>{Status[1]}</Select.Option>
        </Select>
      );
    },
  },
  {
    title: '注册时间',
    dataIndex: 'userRegtime',
    width: 200,
    align: 'center',
    search: false,
  },
  {
    title: '操作',
    align: 'center',
    search: false,
    render: (text, record, _, action) => [
      <Button
        key="editable"
        type="primary"
        icon={<EditOutlined />}
        onClick={() => {
          action?.startEditable?.(record.id);
        }}
      >
        编辑
      </Button>,
    ],
  },
];

const index = () => {
  const [transferOpen, setTransferOpen] = useState<boolean>(false);
  const [columnsConfig, setColumnsConfig] = useState<any[]>(columns);
  const [flag, setFlag] = useState(true);

  const request = async (arg) => {
    arg.size = arg.pageSize;
    delete arg.pageSize;
    const { records, total } = await getList({
      ...arg,
    });
    // console.log(data);

    return {
      data: records,
      success: true,
      total,
    };
  };
  const handleShowChange = (options) => {
    const arr = columns.filter(
      (item) =>
        options.find((val) => val.key === item.dataIndex)?.isShow ||
        item.notColumnShow,
    );
    setColumnsConfig([...arr]);
  };
  return (
    <div>
      <ProTable
        columns={columnsConfig}
        request={request} //点击分页
        rowKey="prodId"
        pagination={{
          pageSize: 5,
        }}
        toolbar={{
          className: classNames(styles.myToolbar),
          settings: [
            {
              icon: <RedoOutlined />,
              tooltip: '刷新',
            },
            {
              icon: <AppstoreOutlined />,
              tooltip: '显隐',
              key: 'show',
              onClick: () => {
                setTransferOpen(true);
              },
            },
            {
              icon: <SearchOutlined />,
              tooltip: '搜索',
              onClick: () => {
                setFlag(!flag);
              },
            },
          ],
        }}
        search={flag}
        
      ></ProTable>
      <ColumnsTransfer
        columns={columns}
        show={transferOpen}
        onShowChange={handleShowChange}
        modalConfig={{
          title: '多选',
          onCancel: () => {
            setTransferOpen(false);
          },
        }}
      />
    </div>
  );
};

export default index;
