import styles from '../style.less';
import { useRef, useState } from 'react';
import { ProColumns, ProTable } from '@ant-design/pro-components';
import ColumnsTransfer from '@/components/columnsTransfer';
import type { RcFile, UploadFile, UploadProps } from 'antd/es/upload/interface';
import {
  getProdCategory,
  addClassifyList,
  showClassifyList,
  editClassifyList,
  deleteClassifyList,
} from '@/services/prod';
import {
  Button,
  Select,
  Tag,
  Modal,
  Form,
  Radio,
  Image,
  Input,
  message,
  Popconfirm,
  Upload,
  InputNumber,
  Cascader,
} from 'antd';
import {
  EditOutlined,
  DeleteOutlined,
  PlusOutlined,
  UploadOutlined,
  LoadingOutlined,
} from '@ant-design/icons';
import { UploadChangeParam } from 'antd/lib/upload';
enum Status {
  正常 = 1,
  下线 = 0,
}

export default function ClassifyManagement() {
  const columns: ProColumns<GithubIssueItem>[] = [
    {
      dataIndex: 'categoryName',
      title: '分类名称',
      width: 200,
      align: 'center',
    },
    {
      dataIndex: 'pic',
      title: '图片',
      width: 200,
      align: 'center',
      render(text, records, index) {
        return (
          <Image
            src={`https://img.mall4j.com/${records.pic}`}
            className={styles.pic}
          />
        );
      },
    },
    {
      dataIndex: 'status',
      title: '状态',
      align: 'center',
      render(text, records, index) {
        return (
          <Tag color={records.status == 1 ? 'blue' : 'orange'}>
            {Status[records.status]}
          </Tag>
        );
      },
    },
    {
      dataIndex: 'seq',
      align: 'center',
      title: '排序号',
    },
    {
      dataIndex: '',
      title: '操作',
      align: 'center',
      width: 300,
      search: false,
      render(text, records, index) {
        return (
          <div>
            <Button
              type="primary"
              icon={<EditOutlined />}
              // className={styles.btn1}
              onClick={() => {
                handelEdit(records);
              }}
            >
              修改
            </Button>
            <Popconfirm
              title="确定进行删除操作？"
              onConfirm={() => handleConfirmOk(records)}
              onCancel={cancel}
              okText="Yes"
              cancelText="No"
            >
              <Button
                type="primary"
                danger
                className={styles.btn}
                icon={<DeleteOutlined />}
              >
                删除
              </Button>
            </Popconfirm>
          </div>
        );
      },
    },
  ];
  const addProdFormRef = useRef(null);
  // const [isModalOpen, setIsModalOpen] = useState(false);
  const [dialogTile, setDialogTile] = useState('');
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const [imageUrl, setImageUrl] = useState<string>();
  const [isAdd, setIsAdd] = useState(false);
  const [open, setOpen] = useState(false);
  const [form] = Form.useForm();
  const [classifyData, setClassifyData] = useState([]);
  const [listData, setListData] = useState([]);
  const request = async (arg: {
    current: any;
    size?: number;
    pageSize?: any;
  }) => {
    const data = await getProdCategory({
      current: arg.current,
      size: arg.pageSize,
    });
    setClassifyData(formateClassifyData(data, 0));
    setListData(formateData(data, 0));
    return {
      data: classifyData,
      success: true,
    };
  };
  // 格式化数据
  const formateClassifyData = (data: any[], parentId: number): any => {
    return data
      .filter((val: { parentId: number }) => val.parentId === parentId)
      .map((item) => {
        return {
          ...item,
          children: formateClassifyData(data, item.categoryId),
        };
      });
  };
  // 格式化数据
  const formateData = (data: any[], parentId: number): any => {
    return data
      .filter((val: any) => Number(val.parentId) === parentId)
      .map((item: any) => {
        return {
          title: item.categoryName,
          value: item.categoryName,
          children: formateData(data, item.categoryId),
        };
      });
  };
  // 新增
  const handelAdd = () => {
    setIsModalOpen(true);
    setDialogTile('新增');
    setIsAdd(true);
    form.resetFields(); //重置数据
  };
  // 编辑
  const handelEdit = async (records: any) => {
    setIsModalOpen(true);
    setDialogTile('修改');
    setIsAdd(false);
    const res = await showClassifyList(records.categoryId);
    setImageUrl('https://img.mall4j.com/' + res.pic);
    form.setFieldsValue(res);
  };

  // 确认按钮
  const handelFinish = async (values: any) => {
    if (isAdd) {
      // 增加
      const res = await addClassifyList({
        ...values,
        id: 0,
        isDefault: null,
        prodCount: null,
        shopId: null,
      });
      res && message.success('新增成功');
    } else {
      // 编辑
      console.log('编辑');
      const res = await editClassifyList();
      res && message.success('编辑成功');
    }
    await request({
      current: 1,
      size: 10,
    });
    // setIsModalOpen(false);
    // addProdFormRef.current.reload();//刷新页面
    form.resetFields(); //重置数据
  };
  const handleConfirmOk = async (records) => {
    console.log(records);
    try {
      const res = await deleteClassifyList(records.categoryId);
      await request({
        current: 1,
        size: 10,
      });
      message.success('删除成功');
      addProdFormRef.current.reload(); //刷新页面
    } catch (error) {
      message.warning('失败了');
    }
  };
  // 二次确认弹框关闭
  const cancel = () => {
    console.log('Clicked cancel button');
    setOpen(false);
  };
  // 关闭弹框
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  const getBase64 = (img: RcFile, callback: (url: string) => void) => {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result as string));
    reader.readAsDataURL(img);
  };

  const beforeUpload = async (file: RcFile) => {
    const isJpgOrPng =
      file.type === 'image/png' ||
      file.type === 'image/jpeg' ||
      file.type === 'image/jpg';
    if (!isJpgOrPng) {
      message.error('你得上传图片');
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.error('图片最大不超过 2MB!');
    }
    return isJpgOrPng && isLt2M;
  };
  const handleChange: UploadProps['onChange'] = (
    info: UploadChangeParam<UploadFile>,
  ) => {
    if (info.file.status === 'uploading') {
      setLoading(true);
      return;
    }
    if (info.file.status === 'done') {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj as RcFile, (url) => {
        setLoading(false);
        setImageUrl(url);
        console.log(url);
      });
    }
  };

  const uploadButton = (
    <div>
      {loading ? <LoadingOutlined /> : <PlusOutlined />}
      <div style={{ marginTop: 8 }}></div>
    </div>
  );
  const onChange = (value: string[]) => {
    console.log(value);
  };

  return (
    <div>
      <div>
        {/* <ColumnsTransfer data={prodList} request={request}></ColumnsTransfer> */}
        <ProTable
          actionRef={addProdFormRef}
          bordered={true}
          search={false}
          pagination={false}
          rowKey="categoryId"
          dataSource={classifyData}
          headerTitle={
            <Button
              key="primary"
              type="primary"
              icon={<PlusOutlined />}
              onClick={handelAdd}
            >
              新增
            </Button>
          }
          columns={columns}
          request={request} //表格初始 点击查询 点击分页
        ></ProTable>
        <Modal
          title={dialogTile}
          open={isModalOpen}
          onCancel={handleCancel}
          footer={null}
        >
          <Form
            name="userForm"
            form={form}
            labelCol={{ span: 4 }}
            wrapperCol={{ span: 14 }}
            layout="horizontal"
            onFinish={handelFinish}
          >
            <Form.Item
              label="分类图片"
              name="pic"
              rules={[{ required: true, message: '分类图片不能为空!' }]}
            >
              <Upload
                name="pic"
                listType="picture-card"
                className="avatar-uploader"
                showUploadList={false}
                action="https://img.mall4j.com/admin/file/upload/element"
                beforeUpload={beforeUpload}
                onChange={handleChange}
              >
                {imageUrl ? (
                  <img src={imageUrl} alt="avatar" style={{ width: '100%' }} />
                ) : (
                  uploadButton
                )}
              </Upload>
            </Form.Item>
            <Form.Item
              label="分类名称"
              name="categoryName"
              rules={[{ required: true, message: '分类名称不能为空!' }]}
            >
              <Input />
            </Form.Item>
            <Form.Item label="上级分类" name="status">
              <Cascader
                options={listData}
                fieldNames={{ label: 'title' }}
                placeholder="请选择"
              />
            </Form.Item>
            <Form.Item label="排序号" name="seq">
              <InputNumber min={0} />
            </Form.Item>
            <Form.Item label="状态" name="status">
              <Radio.Group defaultValue={1}>
                <Radio value={Status['下线']}> {Status[0]}</Radio>
                <Radio value={Status['正常']}>{Status[1]} </Radio>
              </Radio.Group>
            </Form.Item>
            <Form.Item>
              <Button onClick={handleCancel}>取消</Button>
              <Button htmlType="submit" type="primary">
                确定
              </Button>
            </Form.Item>
          </Form>
        </Modal>
      </div>
    </div>
  );
}
