import { ProTable, ProColumns } from '@ant-design/pro-components';
import {
  getProdSpec,
  getProdSpecDel,
  getProdSpecAdd,
  Editnorms,
} from '@/services/prod';
import { Tag, Button, Modal, Input, message, Form } from 'antd';
import {
  RedoOutlined,
  AppstoreOutlined,
  SearchOutlined,
  EditOutlined,
  DeleteOutlined,
  MinusCircleOutlined,
  PlusOutlined,
} from '@ant-design/icons';
import { useState, useRef } from 'react';
import ColumnsTransfer from '@/components/columnsTransfer';
import './index.less';

const ProdSpec = () => {
  interface ProdTableColumns {
    title: string;
  }
  const columns: ProColumns<ProdTableColumns>[] = [
    {
      title: '序号',
      align: 'center',
      dataIndex: '',
      search: false,
      render(text, record, index) {
        return <p>{index + 1}</p>;
      },
    },
    {
      title: '属性名称',
      align: 'center',
      dataIndex: 'propName',
    },
    {
      title: '属性值',
      search: false,
      dataIndex: 'prodPropValues',
      align: 'center',
      render: (text, record, index) => {
        const list = record.prodPropValues.map((item) => {
          return <Tag color="blue">{item.propValue}</Tag>;
        });
        return list;
      },
    },
    {
      title: '操作',
      search: false,
      align: 'center',
      notCulumnShow: true,
      render: (text, record, index) => [
        <Button
          key="link"
          type="primary"
          icon={<EditOutlined />}
          onClick={() => edit(record)}
        >
          编辑
        </Button>,
        <Button
          key="warn"
          type="primary"
          icon={<DeleteOutlined />}
          danger
          onClick={() => deleData(record.propId)}
        >
          删除
        </Button>,
      ],
    },
  ];

  const [titles, setTitles] = useState<string>('新增');
  const [flag, setFlag] = useState(true);
  const [transferopen, setTransferopen] = useState<boolean>(false);
  const [bomb, setBomb] = useState<boolean>(false);
  const [columnsConfig, setColumnsConfig] = useState<any[]>(columns);
  const [obja, setObja] = useState([]);
  const ref = useRef(null);
  const [form] = Form.useForm();
  const [editId, setEditId] = useState(0);
  const { confirm } = Modal;

  const request = async (arg) => {
    arg.size = arg.pageSize;
    delete arg.pageSize;
    const { records, total } = await getProdSpec({
      ...arg,
    });
    return {
      data: records,
      success: true,
      total: total,
    };
  };

  const handleShowChange = (options) => {
    const arr = columns.filter(
      (item) =>
        options.find((val) => val.key === item.dataIndex)?.isShow ||
        item.notCulumnShow,
    );
    setColumnsConfig([...arr]);
  };
  //删除
  const deleData = async (id: number) => {
    confirm({
      title: '你确定要进行[删除]操作吗',
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      async onOk() {
        const res = await getProdSpecDel(id);
        ref.current.reload();
        message.success('删除成功');
      },
      onCancel() {
        console.log('Cancel');
      },
    });
  };

  //添加
  const add = () => {
    setTitles('新增');
    setBomb(true);
  };
  //点击弹框的确定按钮
  const onFinish = async (values: any) => {
    //@ts-ignore
    const prodPropValues = [];
    //@ts-ignore
    values.propValue.forEach((item, index) => {
      prodPropValues.push({
        propValue: item,
        valueId: index === 0 ? 0 : '',
      });
    });
    //新增的情况
    if (titles === '新增') {
      try {
        const res = await getProdSpecAdd({
          //@ts-ignore
          propName: values.propName,
          //@ts-ignore
          prodPropValues,
        });
      } catch (error) {
        console.log(error);
      } //修改的情况
    } else if (titles === '修改') {
      try {
        const res = await Editnorms({
          //@ts-ignore
          propName: values.propName,
          //@ts-ignore
          prodPropValues,
          propId: editId,
        });
      } catch (error) {
        console.log(error);
      }
    }
    ref.current.reload();
    setBomb(false);
  };

  //编辑
  const edit = async (item: any) => {
    setTitles('修改');
    setBomb(true);
    //改变表单的值
    form.setFieldsValue({
      ...item,
      propValue: item.prodPropValues.map((item, index) => {
        return item.propValue;
      }),
    });
    setObja(item.prodPropValues);
    setEditId(item.propId);
  };

  const oncal = () => {
    setBomb(false);
  };

  return (
    <div className="prodList">
      <ProTable
        actionRef={ref}
        rowKey="propId"
        columns={columnsConfig}
        request={request}
        pagination={{
          pageSize: 5,
        }}
        form={{
          searchText: '搜索',
          resetText: '清空',
          span: 7,
        }}
        toolbar={{
          subTitle: [
            <Button key="add" type="primary" className="btn" onClick={add}>
              + 新增
            </Button>,
          ],
          settings: [
            {
              icon: <RedoOutlined />,
              tooltip: '刷新',
              onClick: () => {
                ref.current.reload();
              },
            },
            {
              icon: <AppstoreOutlined />,
              tooltip: '显隐',
              onClick: () => {
                setTransferopen(true);
              },
            },

            {
              icon: <SearchOutlined />,
              tooltip: '搜索',
              onClick: () => {
                setFlag(!flag);
              },
            },
          ],
        }}
        search={flag}
      ></ProTable>

      <ColumnsTransfer
        columns={columns}
        show={transferopen}
        onShowChange={handleShowChange}
        modalConfig={{
          title: '多选',
          onCancel: () => {
            setTransferopen(false);
          },
        }}
      ></ColumnsTransfer>
      <Modal
        destroyOnClose={true}
        visible={bomb}
        onCancel={() => setBomb(false)}
        title={titles}
        width={750}
        footer={null}
      >
        <div>
          <div className="top">
            <p>
              {' '}
              <span>属性名称</span>{' '}
            </p>
            <p>
              <span>属性值</span>
            </p>
          </div>

          <div className="container">
            <Form preserve={false} form={form} onFinish={onFinish}>
              <div className="wrap">
                <div className="header">
                  <Form.Item name="propName">
                    <Input placeholder="请输入内容" name="name" />
                  </Form.Item>
                </div>
                <div className="wrap-main">
                  <Form.List name="propValue">
                    {(fields, { add, remove }, { errors }) => (
                      <>
                        {fields.map((field, index) => (
                          <Form.Item required={false} key={field.key}>
                            <Form.Item
                              {...field}
                              validateTrigger={['onChange', 'onBlur']}
                              noStyle
                            >
                              <Input
                                placeholder="请输入内容"
                                style={{ width: '90%' }}
                              />
                            </Form.Item>
                            {fields.length > 1 ? (
                              <MinusCircleOutlined
                                className="dynamic-delete-button"
                                onClick={() => remove(field.name)}
                              />
                            ) : null}
                          </Form.Item>
                        ))}
                        <Form.Item>
                          <Button
                            type="dashed"
                            onClick={() => add()}
                            style={{ width: '60%' }}
                            icon={<PlusOutlined />}
                          ></Button>
                        </Form.Item>
                      </>
                    )}
                  </Form.List>
                </div>
              </div>

              <Form.Item>
                <Button type="default" onClick={oncal}>
                  取消
                </Button>
                <Button type="primary" htmlType="submit">
                  确定
                </Button>
              </Form.Item>
            </Form>
          </div>
        </div>
      </Modal>
    </div>
  );
};

export default ProdSpec;
