//分组管理
import React, { useRef, useState } from 'react';
import type { ProColumns } from '@ant-design/pro-components';
import { ProTable, TableDropdown } from '@ant-design/pro-components';
import {
  getProdTag,
  getProdTagAdd,
  getProdTagDel,
  getProdTagData,
  getProdTagEdit,
} from '@/services/prod';
import {
  Image,
  Tag,
  Select,
  Button,
  Modal,
  Form,
  Input,
  Radio,
  Popconfirm,
  message,
} from 'antd';
import {
  EditOutlined,
  DeleteOutlined,
  PlusOutlined,
  RedoOutlined,
  AppstoreOutlined,
  SearchOutlined,
} from '@ant-design/icons';
import styles from '../style.less';
import classNames from 'classnames';
import ColumnsTransfer from '@/components/columnsTransfer';
import { PageParams } from '@/services/pagenation';
import { DataIndex } from 'rc-table/lib/interface';

enum Status {
  禁用,
  正常,
}
enum Style {
  一列一个,
  一列两个,
  一列三个,
}
enum Default {
  自定义类型,
  默认类型,
}
interface ProdTableColums {
  status: number;
  title: string;
  dataIndex?: string;
  notColumnShow?: boolean;
}

const index = () => {
  const columns: ProColumns<ProdTableColums>[] = [
    {
      title: '序号',
      // dataIndex: 'index',
      valueType: 'index',
      width: 48,
    },
    {
      title: '标签名称',
      dataIndex: 'title',
    },
    {
      title: '状态',
      dataIndex: 'status',
      render(text, record, index) {
        return record.status === 1 ? (
          <Tag color="blue">{Status[record.status]}</Tag>
        ) : (
          <Tag color="red">{Status[record.status]}</Tag>
        );
      },
      renderFormItem(item, options, form) {
        const handleStatusChange = (value: number) => {
          form.setFieldValue('status', value);
        };
        return (
          <Select placeholder={'请选择状态'} onChange={handleStatusChange}>
            <Select.Option value={Status['禁用']}>{Status[0]}</Select.Option>
            <Select.Option value={Status['正常']}>{Status[1]}</Select.Option>
          </Select>
        );
      },
    },
    {
      title: '默认类型',
      render(text, record, index) {
        return <Tag color="processing">{Default[record.isDefault]}</Tag>;
      },
      search: false,
    },
    {
      title: '排序',
      dataIndex: 'seq',
      search: false,
    },
    {
      title: '操作',
      search: false,
      render: (text, record, _, action) => [
        <Button
          key="editable"
          type="primary"
          icon={<EditOutlined />}
          onClick={() => editShowModal(record)}
          className={styles.edit}
        >
          修改
        </Button>,
        <Popconfirm
          title="您确定要删除吗?"
          onConfirm={async () => {
            await getProdTagDel(record.id);
            ref.current.reload();
          }}
          okButtonProps={{ loading: confirmLoading }}
          onCancel={handleConfirmCancel}
        >
          <Button
            type="primary"
            danger
            icon={<DeleteOutlined />}
            href={record.url}
            target="_blank"
            rel="noopener noreferrer"
            key="del"
          >
            删除
          </Button>
        </Popconfirm>,
      ],
      notColumnShow: true,
    },
  ];
  const [transferOpen, setTransferOpen] = useState<boolean>(false);
  const [columnsConfig, setColumnsConfig] = useState<any[]>(columns);
  const [titles, setTitles] = useState('');
  const [flag, setFlag] = useState(true);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [open, setOpen] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false); //点击二次确认弹框
  const [itemId, setItemId] = useState();
  const ref = useRef();
  const userInfo = useRef();
  //状态数据
  const [value, setValue] = useState(1);
  //列表样式数据
  const [styleValue, setStyleValue] = useState(0);
  //清空后的表单数据
  const userFrom = {
    createTime: '',
    deleteTime: '',
    id: '',
    isDefault: '',
    prodCount: '',
    seq: '',
    shopId: '',
    title: '',
    updateTime: '',
  };

  const request = async (arg: PageParams) => {
    arg.size = arg.pageSize;
    delete arg.pageSize;
    const { records, total } = await getProdTag({
      ...arg,
    });
    return {
      data: records,
      success: true,
      total,
    };
  };
  const handleShowChange = (options: any[]) => {
    const arr = columns.filter(
      (item) =>
        options.find(
          (val: {
            key:
              | ((string | number | (string | number)[]) & DataIndex)
              | undefined;
          }) => val.key === item.dataIndex,
        )?.isShow || item.notColumnShow,
    );
    setColumnsConfig([...arr]);
  };
  // 单选状态
  const changeStatus = (e: RadioChangeEvent) => {
    setValue(e.target.value);
  };
  const changeStyle = (e) => {
    setValue(e.target.value);
  };
  //点击出现新增弹框
  const addShowModal = () => {
    setTitles('新增');
    setIsModalOpen(true);
    console.log(titles);
    setValue(1); //切换状态值
    setStyleValue(0);
    userInfo.current.setFieldsValue({
      status: 1,
      style: 0,
      ...userFrom,
    });
  };
  //点击编辑出现弹框
  const editShowModal = async (record: any) => {
    setTitles('修改');
    setIsModalOpen(true);
    setItemId(record.id);
    const data = await getProdTagData(record.id);
    console.log(data, '编辑回显数据');
    userInfo.current.setFieldsValue({
      ...data,
    });
  };
  //点击弹框的取消按钮
  const handleCancel = () => {
    setIsModalOpen(false);
    userInfo.current.setFieldsValue({
      status: '',
      style: '',
      ...userFrom,
    });
  };

  //点击二次确认框的取消按钮
  const handleConfirmCancel = () => {
    setOpen(false);
  };
  //点击新增弹框的确定按钮
  const onFinish = async (values: any) => {
    console.log(values, '修改传的值');

    if (titles === '新增') {
      const data = await getProdTagAdd({
        id: 0,
        ...values,
      });
      if (data) {
        setIsModalOpen(false);
      }
    } else if (titles === '修改') {
      setIsModalOpen(false);
      await getProdTagEdit({
        id: itemId,
        ...values,
      });
    }
    message.success({
      content: '操作成功',
      style: {
        color: 'rgb(14, 144, 14)',
      },
    });
    userInfo.current.setFieldsValue({
      status: '',
      style: '',
      ...userFrom,
    });
    ref.current.reload();
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <div>
      <ProTable
        columns={columnsConfig}
        actionRef={ref}
        request={request} //点击分页
        rowKey="id"
        pagination={{
          pageSize: 5,
        }}
        form={{
          searchText: '搜索',
          resetText: '清空',
          span: 7,
        }}
        toolbar={{
          subTitle: [
            <Button
              key="add"
              type="primary"
              className="btn"
              onClick={addShowModal}
            >
              + 新增
            </Button>,
          ],
          settings: [
            {
              icon: <RedoOutlined />,
              tooltip: '刷新',
              onClick: () => {
                ref.current.reload();
              },
            },
            {
              icon: <AppstoreOutlined />,
              tooltip: '显隐',
              onClick: () => {
                setTransferOpen(true);
              },
            },

            {
              icon: <SearchOutlined />,
              tooltip: '搜索',
              onClick: () => {
                setFlag(!flag);
              },
            },
          ],
        }}
        search={flag}
        // toolbar={{
        //   className: classNames(styles.myToolbar),
        //   title: [
        //     <Button
        //       key="add"
        //       type="primary"
        //       icon={<PlusOutlined />}
        //       onClick={addShowModal}
        //     >
        //       新增
        //     </Button>,
        //   ],
        //   settings: [
        //     {
        //       icon: <RedoOutlined />,
        //       onClick: () => {
        //         ref.current.reloadAndRest();
        //       },
        //       tooltip: '刷新',
        //     },
        //     {
        //       icon: <AppstoreOutlined />,
        //       tooltip: '显隐',
        //       key: 'show',
        //       onClick: () => {
        //         setTransferOpen(true);
        //       },
        //     },
        //     {
        //       icon: <SearchOutlined />,
        //       tooltip: '搜索',
        //     },
        //   ],
        // }}
      ></ProTable>
      <ColumnsTransfer
        columns={columns}
        show={transferOpen}
        onShowChange={handleShowChange}
        modalConfig={{
          title: '多选',
          onCancel: () => {
            setTransferOpen(false);
          },
        }}
      />
      <Modal
        title={titles}
        open={isModalOpen}
        footer={null}
        onCancel={handleCancel}
      >
        <Form
          ref={userInfo}
          name="basic"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          initialValues={{ remember: true }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item label="标签名称" name="title">
            <Input />
          </Form.Item>

          <Form.Item label="状态" name="status">
            <Radio.Group
              onChange={changeStatus}
              defaultValue={Status['正常']}
              value={value}
            >
              <Radio value={Status['正常']}> 正常 </Radio>
              <Radio value={Status['禁用']}> 禁用 </Radio>
            </Radio.Group>
          </Form.Item>

          <Form.Item label="列表样式" name="style">
            <Radio.Group
              onChange={changeStyle}
              defaultValue={Style['一列一个']}
              value={styleValue}
            >
              <Radio value={Style['一列一个']}> 一列一个 </Radio>
              <Radio value={Style['一列两个']}> 一列两个 </Radio>
              <Radio value={Style['一列三个']}> 一列三个 </Radio>
            </Radio.Group>
          </Form.Item>

          <Form.Item label="排序" name="seq">
            <Input />
          </Form.Item>

          <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
            <Button htmlType="button" onClick={handleCancel}>
              取消
            </Button>
            <Button type="primary" htmlType="submit">
              确定
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
};

export default index;
