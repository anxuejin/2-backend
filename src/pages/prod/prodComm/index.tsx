//评论管理
import React, { useRef, useState } from 'react';
import type { ProColumns } from '@ant-design/pro-components';
import { ProTable, TableDropdown } from '@ant-design/pro-components';
import { getProdComm } from '@/services/prod';
import { Image, Tag, Select, Button } from 'antd';
import {
  EditOutlined,
  DeleteOutlined,
  RedoOutlined,
  AppstoreOutlined,
  SearchOutlined,
  PlusOutlined,
} from '@ant-design/icons';
import styles from '../style.less';
import classNames from 'classnames';
import ColumnsTransfer from '@/components/columnsTransfer';

enum Status {
  未上架,
  上架,
}
interface ProdTableColums {
  title: string;
  dataIndex?: string;
  notColumnShow?: boolean;
}

const index = () => {
  const columns: ProColumns<ProdTableColums>[] = [
    {
      title: '序号',
      // dataIndex: 'index',
      valueType: 'index',
      // width: 48,
      search: false,
    },
    {
      title: '商户名',
      dataIndex: 'prodName',
      width: 200,
    },
    {
      title: '用户昵称',
      dataIndex: 'oriPrice',
      search: false,
    },
    {
      title: '记录时间',
      dataIndex: 'price',
      search: false,
    },
    {
      title: '回复时间',
      dataIndex: 'totalStocks',
      search: false,
    },
    {
      title: '评价得分',
      dataIndex: 'totalStocks',
      search: false,
    },
    {
      title: '是否匿名',
      dataIndex: 'totalStocks',
      search: false,
    },
    {
      title: '审核状态',
      render(text, record, index) {
        return <Tag>{Status[record.status]}</Tag>;
      },
      renderFormItem() {
        return (
          <Select>
            {/* {
            console.log(Status);
            
          } */}
          </Select>
        );
      },
    },
    {
      title: '操作',
      search: false,
      notColumnShow: true,
      render: (text, record, _, action) => [
        <Button
          key="editable"
          type="primary"
          icon={<EditOutlined />}
          onClick={() => {
            action?.startEditable?.(record.id);
          }}
          className={styles.edit}
        >
          编辑
        </Button>,
        <Button
          type="primary"
          danger
          icon={<DeleteOutlined />}
          href={record.url}
          target="_blank"
          rel="noopener noreferrer"
          key="view"
        >
          删除
        </Button>,
      ],
    },
  ];
  const [transferOpen, setTransferOpen] = useState<boolean>(false);
  const [columnsConfig, setColumnsConfig] = useState<any[]>(columns);
  const ref = useRef();
  const [flag, setFlag] = useState<boolean>(true);

  const request = async (arg) => {
    const { records, total } = await getProdComm({
      current: arg.current,
      size: arg.pageSize,
    });
    // console.log(data);

    return {
      data: records,
      success: true,
      total,
    };
  };
  const handleShowChange = (options) => {
    const arr = columns.filter(
      (item) =>
        options.find((val) => val.key === item.dataIndex)?.isShow ||
        item.notColumnShow,
    );
    setColumnsConfig([...arr]);
  };
  return (
    <div>
      <ProTable
        columns={columnsConfig}
        actionRef={ref}
        request={request} //点击分页
        rowKey="prodId"
        pagination={{
          pageSize: 20,
        }}
        form={{
          searchText: '搜索',
          resetText: '清空',
          span: 7,
        }}
        toolbar={{
          subTitle: [
            <Button key="add" type="primary" className="btn">
              + 新增
            </Button>,
          ],
          settings: [
            {
              icon: <RedoOutlined />,
              tooltip: '刷新',
              onClick: () => {
                ref.current.reload();
              },
            },
            {
              icon: <AppstoreOutlined />,
              tooltip: '显隐',
              onClick: () => {
                setTransferopen(true);
              },
            },

            {
              icon: <SearchOutlined />,
              tooltip: '搜索',
              onClick: () => {
                setFlag(!flag);
              },
            },
          ],
        }}
        search={flag}
        // toolbar={{
        //   className: classNames(styles.myToolbar),
        //   title: [
        //     <Button
        //       key="add"
        //       type="primary"
        //       icon={<PlusOutlined />}
        //       onClick={() => {
        //         alert('add');
        //       }}
        //     >
        //       添加
        //     </Button>,
        //     <Button
        //       key="del"
        //       type="primary"
        //       danger
        //       onClick={() => {
        //         alert('del');
        //       }}
        //     >
        //       批量删除
        //     </Button>,
        //   ],
        //   settings: [
        //     {
        //       icon: <RedoOutlined />,
        //       tooltip: '刷新',
        //       onClick: () => {
        //         ref.current.reloadAndRest();
        //       },
        //     },
        //     {
        //       icon: <AppstoreOutlined />,
        //       tooltip: '显隐',
        //       key: 'show',
        //       onClick: () => {
        //         setTransferOpen(true);
        //       },
        //     },
        //     {
        //       icon: <SearchOutlined />,
        //       tooltip: '搜索',
        //     },
        //   ],
        // }}
      ></ProTable>
      <ColumnsTransfer
        columns={columns}
        show={transferOpen}
        onShowChange={handleShowChange}
        modalConfig={{
          title: '多选',
          onCancel: () => {
            setTransferOpen(false);
          },
        }}
      />
    </div>
  );
};

export default index;
