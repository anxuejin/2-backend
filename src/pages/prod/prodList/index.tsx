//产品管理
import React, { useEffect, useRef, useState } from 'react';
import type { ProColumns } from '@ant-design/pro-components';
import { ProTable } from '@ant-design/pro-components';
import { getProdList } from '@/services/prod';
import { Image, Tag, Select, Button } from 'antd';
import { history } from 'umi';
import {
  EditOutlined,
  DeleteOutlined,
  AppstoreOutlined,
  SearchOutlined,
  RedoOutlined,
  PlusOutlined,
} from '@ant-design/icons';
import styles from '../style.less';
import classNames from 'classnames';
import ColumnsTransfer from '@/components/columnsTransfer';

enum Status {
  未上架,
  上架,
}
interface ProdTableColums {
  status: number;
  title: string;
  dataIndex?: string;
  notColumnShow?: boolean;
}

const index = () => {
  const columns: ProColumns<ProdTableColums>[] = [
    {
      title: '产品名字',
      dataIndex: 'prodName',
      width: 200,
    },
    {
      title: '商品原价',
      dataIndex: 'oriPrice',
      search: false,
    },
    {
      title: '商品现价',
      dataIndex: 'price',
      search: false,
    },
    {
      title: '商品库存',
      dataIndex: 'totalStocks',
      search: false,
    },
    {
      title: '产品图片',
      render(text, record, index) {
        return (
          <Image
            width={140}
            src={record?.pic}
            fallback="https://ts1.cn.mm.bing.net/th/id/R-C.d5db82cbbec25fbe9bd559cc82d586fd?rik=kKzks2RY8lCSPg&riu=http%3a%2f%2fbpic.588ku.com%2felement_list_pic%2f19%2f04%2f22%2f53b40a019c5a84ac1aca2a85eea44889.jpg&ehk=4NLlZ%2fhJhYpAhmr8kNXCuCXijJTIkVz6QVq0GsKZPbI%3d&risl=&pid=ImgRaw&r=0&sres=1&sresct=1"
          />
        );
      },
      search: false,
    },
    {
      title: '状态',
      dataIndex: 'status',
      render(text, record, index) {
        return record.status === 1 ? (
          <Tag color="blue">{Status[record.status]}</Tag>
        ) : (
          <Tag color="red">{Status[record.status]}</Tag>
        );
      },
      renderFormItem(item, options, form) {
        const handleStatusChange = (value: number) => {
          form.setFieldValue('status', value);
        };
        return (
          <Select placeholder={'请选择状态'} onChange={handleStatusChange}>
            <Select.Option value={Status['未上架']}>{Status[0]}</Select.Option>
            <Select.Option value={Status['上架']}>{Status[1]}</Select.Option>
          </Select>
        );
      },
    },
    {
      title: '操作',
      search: false,
      render: (text, record, _, action) => [
        <Button
          key="editable"
          type="primary"
          icon={<EditOutlined />}
          className={styles.edit}
          onClick={() => history.push('/prodInfo?prodid=' + record.prodId)}
        >
          编辑
        </Button>,
        <Button
          type="primary"
          danger
          icon={<DeleteOutlined />}
          href={record.url}
          target="_blank"
          rel="noopener noreferrer"
          key="view"
        >
          删除
        </Button>,
      ],
      notColumnShow: true,
    },
  ];
  const [transferOpen, setTransferOpen] = useState<boolean>(false);
  const [columnsConfig, setColumnsConfig] = useState<any[]>(columns);
  const ref = useRef();
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
  const [disabled, setDisabled] = useState<boolean>(true);
  const [flag, setFlag] = useState<boolean>(true);

  const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
    console.log('selectedRowKeys changed: ', newSelectedRowKeys);
    setSelectedRowKeys(newSelectedRowKeys);
    if (newSelectedRowKeys.length > 0) {
      setDisabled(false);
    } else {
      setDisabled(true);
    }
  };
  // useEffect(() => {},[]);
  const rowSelection: any = {
    selectedRowKeys,
    onChange: onSelectChange,
  };
  const request = async (arg) => {
    arg.size = arg.pageSize;
    delete arg.pageSize;
    const { records, total } = await getProdList({
      ...arg,
    });

    return {
      data: records,
      success: true,
      total,
    };
  };
  const handleShowChange = (options) => {
    const arr = columns.filter(
      (item) =>
        options.find((val) => val.key === item.dataIndex)?.isShow ||
        item.notColumnShow,
    );
    setColumnsConfig([...arr]);
  };

  return (
    <div>
      <ProTable
        columns={columnsConfig}
        rowSelection={rowSelection}
        actionRef={ref}
        request={request} //点击分页
        rowKey="prodId"
        pagination={{
          pageSize: 5,
        }}
        form={{
          searchText: '搜索',
          resetText: '清空',
          span: 7,
        }}
        toolbar={{
          subTitle: [
            <Button
              key="add"
              type="primary"
              className="btn"
              onClick={() => {
                history.push('/prodInfo');
              }}
            >
              + 新增
            </Button>,
          ],
          settings: [
            {
              icon: <RedoOutlined />,
              tooltip: '刷新',
              onClick: () => {
                ref.current.reload();
              },
            },
            {
              icon: <AppstoreOutlined />,
              tooltip: '显隐',
              onClick: () => {
                setTransferOpen(true);
              },
            },

            {
              icon: <SearchOutlined />,
              tooltip: '搜索',
              onClick: () => {
                setFlag(!flag);
              },
            },
          ],
        }}
        search={flag}
        // toolbar={{
        //   className: classNames(styles.myToolbar),
        //   title: [
        //     <Button
        //       key="add"
        //       type="primary"
        //       icon={<PlusOutlined />}
        //       onClick={() => {
        //         history.push('/prodInfo');
        //       }}
        //     >
        //       新增
        //     </Button>,
        //     <Button
        //       key="del"
        //       type="primary"
        //       danger
        //       onClick={() => {
        //         alert('del');
        //       }}
        //       disabled={disabled}
        //     >
        //       批量删除
        //     </Button>,
        //   ],
        //   settings: [
        //     {
        //       icon: <RedoOutlined />,
        //       tooltip: '刷新',
        //       onClick: () => {
        //         ref.current.reloadAndRest();
        //       },
        //     },
        //     {
        //       icon: <AppstoreOutlined />,
        //       tooltip: '显隐',
        //       key: 'show',
        //       onClick: () => {
        //         setTransferOpen(true);
        //       },
        //     },
        //     {
        //       icon: <SearchOutlined />,
        //       tooltip: '搜索',
        //     },
        //   ],
        // }}
      ></ProTable>
      <ColumnsTransfer
        columns={columns}
        show={transferOpen}
        onShowChange={handleShowChange}
        modalConfig={{
          title: '多选',
          onCancel: () => {
            setTransferOpen(false);
          },
        }}
      />
    </div>
  );
};

export default index;
