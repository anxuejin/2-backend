import React, { useState, useMemo } from 'react';
import { ProFormText, LoginForm } from '@ant-design/pro-components';
import style from '../index.less';
import { v4 as uuidv4 } from 'uuid';
import classNames from 'classnames';
import { login } from '../../services/user';
import LocalStore from '../../utils/storage';
export const userLocalStore = new LocalStore({
  type: 'local',
  name: 'userInfo',
});
interface LoginFormData {
  principal: string;
  credentials: string;
  imageUrl: string;
  sessionUUID?: string;
}
const Login = () => {
  const [codeImageLoading, setCodeImageLoading] = useState(true);
  const [getCode, setgetCode] = useState(() => uuidv4());
  const imageUrl = useMemo(
    () => `https://bjwz.bwie.com/mall4w/captcha.jpg?uuid=${getCode}`,
    [getCode],
  );
  //点击验证码更换
  const onChangeCode = async () => {
    setgetCode(uuidv4());
    setCodeImageLoading(true);
  };
  //点击登录
  const onFinish = async (values: LoginFormData) => {
    console.log(values, '登录');

    values = {
      ...values,
      sessionUUID: getCode,
    };
    const data = await login(values);
    window.localStorage.setItem(
      'userInfo',
      JSON.stringify({
        token: data.access_token,
        tokenType: data.token_type,
        expires_in: Date.now() + data.expires_in * 1000,
      }),
    );

    window.location.href = '/';
  };
  return (
    <div id={style.login}>
      <LoginForm
        logo="https://img5.tianyancha.com/logo/lll/ae0a32f03df9dd999db480baef125c18.png@!f_200x200"
        title="&nbsp;八维电商后台"
        subTitle="&emsp;&emsp;&emsp;八维电商后台管理系统"
        onFinish={onFinish}
      >
        <ProFormText
          name="principal"
          fieldProps={{
            size: 'small',
          }}
          placeholder={'请输入用户名'}
          rules={[
            {
              required: true,
              message: '用户名不能为空!',
            },
          ]}
        />
        <ProFormText.Password
          name="credentials"
          fieldProps={{
            size: 'small',
          }}
          placeholder={'请输入密码'}
          rules={[
            {
              required: true,
              message: '密码不能为空！',
            },
          ]}
        />
        <div className={style.captcha}>
          <div>
            <ProFormText
              name="imageCode"
              placeholder={'请输入验证码'}
              rules={[
                {
                  required: true,
                  message: '验证码不能为空！',
                },
              ]}
            ></ProFormText>
          </div>
          <div
            className={classNames(
              {
                [style.loading]: codeImageLoading,
              },
              style.code,
            )}
          >
            <img
              src={imageUrl}
              onClick={onChangeCode}
              className={style.captchaImg}
              onError={() => {
                setCodeImageLoading(false);
              }}
              onLoad={() => {
                setCodeImageLoading(false);
              }}
            />
          </div>
        </div>
      </LoginForm>
    </div>
  );
};

export default Login;
