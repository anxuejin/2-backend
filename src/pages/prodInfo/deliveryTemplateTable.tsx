const colums = [
  {
    title: '配送区域',
    dataIndex: 'cityList',
    render(text, record) {
      return (
        <span>
          {record.cityList?.length ? record.cityList.join('/') : '所有区域'}
        </span>
      );
    },
  },
  {
    title: '首件(个)',
    dataIndex: 'firstPiece',
  },
  {
    title: '运费(元)',
    dataIndex: 'firstFee',
  },
  {
    title: '续件(个)',
    dataIndex: 'continuousPiece',
  },
  {
    title: '续费(元)',
    dataIndex: 'continuousFee',
  },
];
export default colums;
