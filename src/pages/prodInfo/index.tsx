import React, { useEffect, useState } from 'react';
import { history } from 'umi';
import {
  Form,
  Upload,
  Radio,
  Cascader,
  Input,
  Checkbox,
  Button,
  Table,
} from 'antd';
import {
  getCategoryList,
  getTagList,
  getTransportList,
  getTransportId,
} from '@/services/prod';
import deliveryTemplateColums from './deliveryTemplateTable';
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';

const formatCategoryData = (data: any, parentId: number) => {
  return data
    .filter((val) => Number(val.parentId) === parentId)
    .map((item) => {
      return {
        ...item,
        children: formatCategoryData(data, item.categoryId),
      };
    });
};
const index = () => {
  const [form] = Form.useForm();
  //用户选择的数据
  const [UserFormData, setUserFormData] = useState({
    deliveryMode: [],
    uploadImageLoading: false,
    deliveryTemplateId: null,
    deliveryTemplateTable: [],
    // uploadImageLoading: true,
  });
  //表单初始数据
  const [formData, setFormData] = useState({});
  const init = async (prodid: string | undefined) => {
    if (prodid) {
      //修改
    }
    const [CategoryList, TagList, TransportList] = await Promise.allSettled([
      getCategoryList(),
      getTagList(),
      getTransportList(),
    ]);
    form.setFieldsValue({
      categoryId: [],
      tagList: [],
    });
    setFormData({
      CategoryList: formatCategoryData(CategoryList.value, 0),
      TagList: TagList.value,
      TransportList: TransportList.value,
    });
  };
  useEffect(() => {
    const { location } = history;
    init(location.query?.prodid);
  }, []);
  const deliveryModeOptions = [
    { label: '商家配送', value: 'hasUserPickUp' },
    { label: '用户自提', value: 'hasShopDelivery' },
  ];
  const uploadButton = (
    <div>
      {UserFormData.uploadImageLoading ? <LoadingOutlined /> : <PlusOutlined />}
      <div style={{ marginTop: 8 }}>Upload</div>
    </div>
  );
  return (
    <Form>
      <Form.Item name="pic" label={'产品图片'}>
        <Upload className="avatar-uploader">{uploadButton}</Upload>
      </Form.Item>
      <Form.Item name="status" label={'产品状态'}>
        <Radio.Group defaultValue={1}>
          <Radio value={1}>上架</Radio>
          <Radio value={0}>下架</Radio>
        </Radio.Group>
      </Form.Item>
      <Form.Item name="cetagoryId" label={'产品分类'}>
        <Cascader
          options={formData?.CategoryList}
          fieldNames={{ label: 'categoryName', value: 'categoryId' }}
        />
      </Form.Item>
      <Form.Item name="tagList" label={'产品分组'}>
        <Cascader
          options={formData?.TagList}
          multiple
          fieldNames={{ label: 'title', value: 'id' }}
        />
      </Form.Item>
      <Form.Item
        name="prodName"
        label={'产品名称'}
        rules={[{ required: true, message: '请输入产品名称' }]}
      >
        <Input />
      </Form.Item>
      <Form.Item name="brief" label={'产品卖点'}>
        <Input.TextArea />
      </Form.Item>
      <Form.Item name="deliveryMode" label={'配送方式'}>
        <Checkbox.Group
          options={deliveryModeOptions}
          onChange={(val) => {
            setUserFormData({
              ...UserFormData,
              deliveryMode: val,
              deliveryTemplateId: val.includes('hasUserPickUp')
                ? UserFormData.deliveryTemplateId
                : null,
              deliveryTemplateTable: val.includes('hasUserPickUp')
                ? UserFormData.deliveryTemplateTable
                : [],
            });
          }}
        />
      </Form.Item>
      {UserFormData.deliveryMode.includes('hasUserPickUp') && (
        <Form.Item
          name="deliveryTemplateId"
          label={'运费设置'}
          rules={[{ required: true, message: '请选择运费设置' }]}
        >
          <Cascader
            options={formData?.TransportList}
            placeholder="请选择运费设置模板"
            fieldNames={{ label: 'transName', value: 'transportId' }}
            onChange={async ([id]) => {
              setUserFormData({
                ...UserFormData,
                deliveryTemplateTableLoading: true,
              });
              const { transfees } = await getTransportId(id);
              setUserFormData({
                ...UserFormData,
                deliveryTemplateId: id,
                deliveryTemplateTable: transfees,
                deliveryTemplateTableLoading: false,
              });
            }}
          />
          {UserFormData.deliveryTemplateId && (
            <Table
              dataSource={UserFormData.deliveryTemplateTable}
              loading={UserFormData.deliveryTemplateTableLoading}
              columns={deliveryTemplateColums}
              pagination={false}
            ></Table>
          )}
        </Form.Item>
      )}

      <Form.Item name="skuList" label={'商品规格'}>
        <Button>添加规格</Button>
      </Form.Item>
      <Form.Item name="content" label={'产品详情'}></Form.Item>
    </Form>
  );
};

export default index;
