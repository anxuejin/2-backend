import styles from './index.less';
import { getMenu } from '@/services/sys';
import { useEffect, useState, useRef } from 'react';
import { ProTable } from '@ant-design/pro-components';
import { Button, Tag } from 'antd';
import type { ProColumns } from '@ant-design/pro-components';
import { RightOutlined, DownOutlined } from '@ant-design/icons';

interface MenuItem {
  menuId: number;
  name: string;
  parentId: number;
  icon: string;
  orderNum: number;
  type: number;
  perms: string;
  url: string;
}
interface TreeItem {
  menuId: number;
  name: string;
  icon: string;
  orderNum: number;
  type: number;
  perms: string;
  url: string;
  children: Array<TreeItem>;
}

enum Type {
  目录 = 0,
  菜单 = 1,
  按钮 = 2,
}

// enum Icon {
//   admin = '<UserOutlined />',
//   store = '<ShopOutlined />',
//   vip = '<CrownOutlined />',
//   order = '<FileTextOutlined />',
//   system = '<SettingOutlined />'
// }

// 格式化数据
const formMenuData = (data: MenuItem[], parentId: number): TreeItem[] => {
  return data
    .filter((val) => Number(val.parentId) === parentId)
    .map((item: MenuItem) => {
      return {
        menuId: item.menuId,
        name: item.name,
        icon: item.icon,
        orderNum: item.orderNum,
        type: item.type,
        perms: item.perms,
        url: item.url,
        children: formMenuData(data, item.menuId),
      };
    });
};

export default function IndexPage() {
  const columns: ProColumns[] = [
    {
      title: '名称',
      dataIndex: 'name',
      width: 150,
      align: 'center',
    },
    {
      title: '图标',
      dataIndex: 'icon',
      width: 150,
      align: 'center',
      // render(text, records, index){
      //   return <span>{Icon[records.icon]}</span>
      // }
    },
    {
      title: '类型',
      dataIndex: 'type',
      width: 150,
      align: 'center',
      render(text, records, index) {
        return records.type === 0 ? (
          <Tag color="processing">{Type[records.type]}</Tag>
        ) : records.type === 1 ? (
          <Tag color="success">{Type[records.type]}</Tag>
        ) : (
          <Tag>{Type[records.type]}</Tag>
        );
      },
    },
    {
      title: '排序号',
      dataIndex: 'orderNum',
      width: 150,
      align: 'center',
    },
    {
      title: '菜单URL',
      dataIndex: 'url',
      width: 200,
      align: 'center',
    },
    {
      title: '授权标识',
      dataIndex: 'perms',
      width: 200,
      align: 'center',
    },
    {
      title: '操作',
      render(text, records, index) {
        return (
          <div className="btn">
            <Button
              style={{
                fontSize: '12px',
                marginRight: '10px',
                border: 'none',
                color: '#02a1e9',
              }}
            >
              修改
            </Button>
            <Button
              style={{ fontSize: '12px', border: 'none', color: '#02a1e9' }}
            >
              删除
            </Button>
          </div>
        );
      },
      align: 'center',
    },
  ];

  // 刷新
  const ref = useRef();

  // 表格数据
  const request = async () => {
    const res = await getMenu();
    return {
      data: formMenuData(res, 0),
      success: true,
    };
  };
  return (
    <div className="IndexStyle">
      <ProTable
        actionRef={ref}
        rowKey="menuId"
        toolbar={{
          title: [
            <Button
              key="key"
              type="primary"
              style={{ marginRight: '10px', fontSize: '12px' }}
            >
              新增
            </Button>,
          ],
          settings: [],
        }}
        search={false}
        bordered={true}
        columns={columns}
        request={request}
        // expandable={{
        //   expandIcon: ({ expanded, onExpand, record }) =>
        //     expanded ? (
        //       <RightOutlined onClick={(e) => onExpand(record, e)} />
        //     ) : (
        //       <DownOutlined onClick={(e) => onExpand(record, e)} />
        //     ),
        //   rowExpandable: (record) =>
        //     record.children.length !== 0 ? true : false,
        // }}
      ></ProTable>
    </div>
  );
}
