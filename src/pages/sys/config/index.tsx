import React, { useState } from 'react';
import type { ProColumns } from '@ant-design/pro-components';
import { ProTable } from '@ant-design/pro-components';

import {
  EditOutlined,
  DeleteOutlined,
  PlusOutlined,
  RedoOutlined,
  AppstoreOutlined,
  SearchOutlined,
} from '@ant-design/icons';
import classNames from 'classnames';
import styles from '../../prod/style.less';
import ColumnsTransfer from '@/components/columnsTransfer';
import { getShan, getCan, getZen } from '@/services/prod';
import {
  Image,
  Tag,
  Select,
  Button,
  Modal,
  Form,
  Input,
  Radio,
  Popconfirm,
  message,
} from 'antd';
import { PageParams } from '@/services/pagenation';
import { DataIndex } from 'rc-table/lib/interface';
interface ProdTableColums {
  title: string;
  dataIndex?: string;
}

const index = () => {
  const columns: ProColumns<ProdTableColums>[] = [
    {
      title: '参数名',
      dataIndex: 'paramKey',
      width: 200,
      align: 'center',
    },
    {
      title: '参数值',
      dataIndex: 'paramValue',
      width: 200,
      search: false,
      align: 'center',
    },
    {
      title: '备注',
      dataIndex: 'remark',
      width: 200,
      search: false,
      align: 'center',
    },
    {
      title: '操作',
      search: false,
      align: 'center',
      render: (text, record, _, action) => [
        <Button
          key="editable"
          type="primary"
          icon={<EditOutlined />}
          onClick={() => {
            action?.startEditable?.(record.id);
          }}
          className={styles.edit}
        >
          编辑
        </Button>,
        <Popconfirm
          title="您确定要删除吗?"
          // onConfirm={handleConfirm(record.id)}
          onConfirm={async () => {
            await getShan([record.id]);
            window.location.reload();
          }}
          okButtonProps={{ loading: confirmLoading }}
          onCancel={handleConfirmCancel}
        >
          <Button
            type="primary"
            danger
            icon={<DeleteOutlined />}
            href={record.url}
            target="_blank"
            rel="noopener noreferrer"
            key="view"
          >
            删除
          </Button>
        </Popconfirm>,
      ],
      notColumnShow: true,
    },
  ];

  const [transferOpen, setTransferOpen] = useState<boolean>(false);
  const [columnsConfig, setColumnsConfig] = useState<any[]>(columns);
  const [titles, setTitles] = useState('新增');
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [open, setOpen] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false); //点击二次确认弹框
  const [flag, setFlag] = useState(true);

  const request = async (arg) => {
    arg.size = arg.pageSize;
    delete arg.pageSize;
    const { records, total } = await getCan({
      ...arg,
    });
    // console.log(data);

    return {
      data: records,
      success: true,
      total,
    };
  };
  const handleShowChange = (options) => {
    const arr = columns.filter(
      (item) =>
        options.find(
          (val: {
            key:
              | ((string | number | (string | number)[]) & DataIndex)
              | undefined;
          }) => val.key === item.dataIndex,
        )?.isShow || item.notColumnShow,
    );
    setColumnsConfig([...arr]);
  };
  //点击出现新增弹框
  const addShowModal = () => {
    setTitles('新增');
    setIsModalOpen(true);
  };

  //点击弹框的取消按钮
  const handleCancel = () => {
    setIsModalOpen(false);
  };

  //点击二次确认框的取消按钮
  const handleConfirmCancel = () => {
    setOpen(false);
  };
  //点击新增弹框的确定按钮
  const onFinish = async (values: any) => {
    // console.log('Success:', values);
    const data = await getZen({
      ...values,
    });
    if (data) {
      message.success(data.success);
      setIsModalOpen(false);
    }
    window.location.reload();
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };
  return (
    <div>
      <ProTable
        columns={columnsConfig}
        request={request} //点击分页
        rowKey="prodId"
        pagination={{
          pageSize: 5,
        }}
        rowSelection={{}}
        toolbar={{
          className: classNames(styles.myToolbar),
          title: [
            <Button
              key="add"
              type="primary"
              icon={<PlusOutlined />}
              onClick={addShowModal}
            >
              新增
            </Button>,
          ],
          settings: [
            {
              icon: <RedoOutlined />,
              tooltip: '刷新',
            },
            {
              icon: <AppstoreOutlined />,
              tooltip: '显隐',
              key: 'show',
              onClick: () => {
                setTransferOpen(true);
              },
            },
            {
              icon: <SearchOutlined />,
              tooltip: '搜索',
              onClick: () => {
                setFlag(!flag);
              },
            },
          ],
        }}
        search={flag}
      ></ProTable>
      <ColumnsTransfer
        columns={columns}
        show={transferOpen}
        onShowChange={handleShowChange}
        modalConfig={{
          title: '多选',
          onCancel: () => {
            setTransferOpen(false);
          },
        }}
      />
      <Modal
        title={titles}
        open={isModalOpen}
        // onOk={handleModal}
        // onCancel={handleCancel}
        footer={null}
      >
        <Form
          name="basic"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          initialValues={{ remember: true }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item label="参数名" name="paramKey">
            <Input />
          </Form.Item>

          <Form.Item label="参数值" name="paramValue">
            <Input />
          </Form.Item>

          <Form.Item label="备注" name="remark">
            <Input />
          </Form.Item>

          <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
            <Button htmlType="button" onClick={handleCancel}>
              取消
            </Button>
            <Button type="primary" htmlType="submit">
              确定
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
};

export default index;
