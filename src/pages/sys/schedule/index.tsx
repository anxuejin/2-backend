import { ProTable } from '@ant-design/pro-components';
import type { ProColumns } from '@ant-design/pro-components';
import {
  getSchedule,
  delSchedule,
  stopSchedule,
  recoverySchedule,
  impSchedule,
  getScheduleInfo,
  editSchedule,
  addSchedule,
} from '@/services/sys';
import { Button, Modal, Form, Input, message, Tag } from 'antd';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import style from './index.less';
import './index.less';
import React, { useState, useRef, useEffect } from 'react';
interface ProdTableColumns {
  title: string;
  dataIndex?: string;
  notColumnShow?: boolean;
}
enum Status { //用户状态
  正常 = 0,
  暂停 = 1,
}
const { confirm } = Modal;

const index: React.FC = () => {
  const columns: ProColumns<ProdTableColumns>[] = [
    {
      title: 'ID',
      dataIndex: 'jobId',
      search: false,
      align: 'center',
    },
    {
      title: 'bean名称',
      dataIndex: 'beanName',
      align: 'center',
    },
    {
      title: '方法名称',
      dataIndex: 'methodName',
      search: false,
      align: 'center',
    },
    {
      title: '参数',
      dataIndex: 'params',
      search: false,
      align: 'center',
    },
    {
      title: 'cron表达式',
      dataIndex: 'cronExpression',
      search: false,
      align: 'center',
    },
    {
      title: '备注',
      dataIndex: 'remark',
      search: false,
      align: 'center',
    },
    {
      title: '状态',
      dataIndex: 'status',
      search: false,
      render(text, record, index) {
        return (
          <Tag
            color={Status[record.status] === '暂停' ? 'error' : 'processing'}
          >
            {Status[record.status]}
          </Tag>
        );
      },
      align: 'center',
    },
    {
      title: '操作',
      render(text, record, index) {
        return (
          <p className={style.tag}>
            <Button type="link" onClick={() => editModal(record)}>
              修改
            </Button>
            <Button
              type="link"
              onClick={() => changeBut(record, '删除', 'del')}
            >
              删除
            </Button>
            <Button
              type="link"
              onClick={() => changeBut(record, '暂停', 'stop')}
            >
              暂停
            </Button>
            <br />
            <Button
              type="link"
              onClick={() => changeBut(record, '恢复', 'recovery')}
            >
              恢复
            </Button>
            <Button
              type="link"
              onClick={() => changeBut(record, '立即执行', 'imp')}
            >
              立即执行
            </Button>
          </p>
        );
      },
      search: false,
      align: 'center',
    },
  ];

  // 编辑每项的status
  const [status, setStatus] = useState();
  // 编辑每项的userId
  const [itemsId, setItemId] = useState();
  // 对话框显隐
  const [isModalOpen, setIsModalOpen] = useState(false);
  // 表格标题
  const [title, setTitle] = useState('');
  // 输入框内容
  // const [beanValue,setBeanValue] = useState("");
  // 批量删除选中项
  const [selectRowkeys, setSelectRowkeys] = useState<React.Key[]>([]);
  // 反显的数据
  const userInfo = useRef();
  const ref = useRef();

  // 新增
  const addModal = async () => {
    setTitle('新增');
    setIsModalOpen(true);
  };
  // 修改回显
  const editModal = async (record: any) => {
    console.log(record);
    setTitle('修改');
    setIsModalOpen(true);
    setItemId(record.jobId);
    setStatus(record.status);
    const data = await getScheduleInfo(record.jobId);
    userInfo.current.setFieldsValue({
      ...data,
    });
  };
  // 点击确定
  const handleOk = () => {
    setIsModalOpen(false);
  };
  // 点击取消
  const handleCancel = () => {
    setIsModalOpen(false);
    userInfo.current.resetFields();
  };
  // 弹框表单提交
  const onFinish = async (values: any) => {
    console.log(values);
    if (title === '修改') {
      await editSchedule({
        jobId: itemsId,
        status: status,
        ...values,
      });
    } else {
      await addSchedule({ ...values });
    }
    message.success({
      content: '操作成功',
      style: {
        color: 'rgb(14, 144, 14)',
      },
    });
    userInfo.current.resetFields();
    ref.current.reload();
  };
  // 查询
  // const search = async () => {
  //   const data = await getSchedule({
  //     page: 1,
  //     limit: 10,
  //     beanName: beanValue,
  //   });
  //   // ref.current.setFieldsValue({...data})
  // }
  // const searchInput = (e:any) => {
  // setBeanValue(e.target.value);
  // }
  // 多选选中项
  const onSelectChange = (allSelectRowkeys: React.Key[]) => {
    setSelectRowkeys(allSelectRowkeys);
  };
  const rowSelection = {
    onChange: onSelectChange,
  };

  // 单个/批量操作(删除/暂停/恢复/执行)
  const changeBut = (record: any, msg: string, type: string) => {
    console.log(record, msg, type);
    confirm({
      title: '提示',
      icon: <ExclamationCircleOutlined />,
      content: `确定对[id=${
        record.jobId ? record.jobId : selectRowkeys
      }]进行[${msg}]操作?`,
      okText: '确定',
      okType: 'primary',
      cancelText: '取消',
      closable: true, //右上角的关闭按钮
      maskClosable: true, //点击蒙层是否允许关闭
      centered: true, //垂直居中展示 Modal
      maskStyle: { background: 'rgba(255,255,255,.5)' }, //遮罩样式
      async onOk() {
        // 删除
        if (type === 'del') {
          await delSchedule(record.jobId ? [record.jobId] : selectRowkeys);
        }
        // 暂停
        if (type === 'stop') {
          await stopSchedule(record.jobId ? [record.jobId] : selectRowkeys);
        }
        // 恢复
        if (type === 'recovery') {
          await recoverySchedule(record.jobId ? [record.jobId] : selectRowkeys);
        }
        // 立即执行
        if (type === 'imp') {
          await impSchedule(record.jobId ? [record.jobId] : selectRowkeys);
        }
        message.success({
          content: '操作成功',
          style: {
            color: 'rgb(14, 144, 14)',
          },
        });
        ref.current.reload();
      },
    });
  };

  // 数据请求
  const request = async (reg: any) => {
    console.log(reg);
    // 表格数据
    const { records, total } = await getSchedule({
      page: reg.current,
      limit: reg.pageSize,
      beanName: reg.beanName,
    });
    // console.log(records);
    return {
      data: records,
      success: true,
      total,
    };
  };
  return (
    <>
      <ProTable
        actionRef={ref}
        // pagination={{
        //   // page:1,
        //   pageSize: 10,
        // }}
        columns={columns}
        request={request} // 表格初始 点击查询 点击分页
        rowKey="jobId"
        bordered={true}
        rowSelection={rowSelection}
        // 重置
        onReset={() => {
          // alert("12121")
        }}
        // 搜索
        form={{
          searchText: '查询',
          resetText: '清空',
        }}
        toolbar={{
          // 标题栏
          // title: [<Input allowClear placeholder="bean名称" onInput={searchInput}/>],
          subTitle: [
            <>
              {/* <Button key="search" onClick={request}>查询</Button>&emsp; */}
              <Button key="add" type="primary" onClick={addModal}>
                新增
              </Button>
              &emsp;
              <Button
                key="delAll"
                type="primary"
                danger
                onClick={() => changeBut({}, '批量删除', 'del')}
                disabled={!selectRowkeys.length > 0}
              >
                批量删除
              </Button>
              &emsp;
              <Button
                key="stopAll"
                type="primary"
                danger
                onClick={() => changeBut({}, '批量暂停', 'stop')}
                disabled={!selectRowkeys.length > 0}
              >
                批量暂停
              </Button>
              &emsp;
              <Button
                key="recoveryAll"
                type="primary"
                danger
                onClick={() => changeBut({}, '批量恢复', 'recovery')}
                disabled={!selectRowkeys.length > 0}
              >
                批量恢复
              </Button>
              &emsp;
              <Button
                key="impAll"
                type="primary"
                danger
                onClick={() => changeBut({}, '批量立即执行', 'imp')}
                disabled={!selectRowkeys.length > 0}
              >
                批量立即执行
              </Button>
              &emsp;
              <Button key="logList" type="primary" className={style.gren}>
                日志列表
              </Button>
            </>,
          ],
          settings: [{ icon: false }, { icon: false }, { icon: false }],
        }}
      ></ProTable>
      <Modal
        title={title}
        open={isModalOpen}
        footer={null}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        <Form
          name="basic"
          ref={userInfo}
          labelCol={{ span: 5 }}
          autoComplete="off"
          onFinish={onFinish}
          initialValues={{ remember: true }}
        >
          <Form.Item
            label="bean名称"
            name="beanName"
            rules={[{ required: true, message: 'beanName不能为空' }]}
          >
            <Input placeholder="spring bean名称,如:testTask" />
          </Form.Item>

          <Form.Item
            label="方法名称"
            name="methodName"
            rules={[{ required: true, message: '方法名称不能为空' }]}
          >
            <Input placeholder="方法名称" />
          </Form.Item>
          <Form.Item label="参数" name="params">
            <Input placeholder="参数" />
          </Form.Item>
          <Form.Item
            label="cron表达式"
            name="cronExpression"
            rules={[{ required: true, message: 'cron表达式不能为空' }]}
          >
            <Input placeholder="如:0 0 12 * * ?" />
          </Form.Item>
          <Form.Item label="备注" name="remark">
            <Input placeholder="备注" />
          </Form.Item>
          <Form.Item wrapperCol={{ offset: 15, span: 16 }}>
            <Button onClick={handleCancel}>取消</Button>
            <Button type="primary" htmlType="submit" onClick={handleOk}>
              确定
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};

export default index;
