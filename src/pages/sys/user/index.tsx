import { ProTable } from '@ant-design/pro-components';
import type { ProColumns } from '@ant-design/pro-components';
import {
  getUserList,
  getRoleList,
  getUserInfo,
  editUser,
  addUser,
  delUser,
} from '@/services/sys';
import type { RadioChangeEvent } from 'antd';
import {
  Button,
  Modal,
  Form,
  Input,
  Radio,
  Checkbox,
  Tag,
  message,
} from 'antd';
import {
  EditOutlined,
  DeleteOutlined,
  PlusOutlined,
  SearchOutlined,
  AppstoreOutlined,
  RedoOutlined,
  ExclamationCircleOutlined,
} from '@ant-design/icons';
import style from './index.less';
import React, { useState, useRef } from 'react';
import ColumnsTransfer from '@/components/columnsTransfer';
interface ProdTableColumns {
  title: string;
  dataIndex?: string;
  notColumnShow?: boolean;
}
enum Status { //用户状态
  禁用 = 0,
  正常 = 1,
}
const { confirm } = Modal;
const index: React.FC = () => {
  const columns: ProColumns<ProdTableColumns>[] = [
    {
      title: '用户名',
      dataIndex: 'username',
      align: 'center',
    },
    {
      title: '邮箱',
      dataIndex: 'email',
      search: false,
      align: 'center',
    },
    {
      title: '手机号',
      dataIndex: 'mobile',
      search: false,
      align: 'center',
    },
    {
      title: '创建时间',
      dataIndex: 'createTime',
      search: false,
      align: 'center',
    },
    {
      title: '状态',
      dataIndex: 'status',
      render(text, record, index) {
        return <Tag color="processing">{Status[record.status]}</Tag>;
      },
      search: false,
      align: 'center',
    },
    {
      title: '操作',
      render(text, record, index) {
        return (
          <li>
            <Button type="primary" onClick={() => editModal(record)}>
              <EditOutlined /> <span>编辑</span>
            </Button>
            &emsp;
            <Button type="primary" danger onClick={() => del(record)}>
              <DeleteOutlined /> <span>删除</span>
            </Button>
          </li>
        );
      },
      search: false,
      notColumnShow: true,
      align: 'center',
    },
  ];
  // 批量删除选中项
  const [selectRowkeys, setSelectRowkeys] = useState<React.Key[]>([]);
  // 编辑每项的userId
  const [itemsId, setItemId] = useState();
  // 表格标题
  const [title, setTitle] = useState('');
  // 角色信息
  const [roleName, setRoleName] = useState<any[]>();
  // 状态单选更改
  const [value, setValue] = useState(1);
  // 高级筛选栏显示隐藏
  const [search, setSearch] = useState<boolean>(true);
  // 标题栏显隐
  const [transferOpen, setTransferOpen] = useState<boolean>(false);
  const [columnsConfig, setColumnsConfig] = useState<any[]>(columns);
  // 对话框显隐
  const [isModalOpen, setIsModalOpen] = useState(false);
  // 反显的数据
  const userInfo = useRef();
  const ref = useRef();
  // 清空后的表单数据
  const userFrom = {
    createTime: '',
    email: '',
    mobile: '',
    roleIdList: [],
    shopId: '',
    userId: '',
    username: '',
    password: '',
    pwd: '',
  };
  // 新增
  const addModal = async () => {
    setTitle('新增');
    setIsModalOpen(true);
    // 角色信息多选框获取
    const res = await getRoleList();
    setRoleName(res);
    setValue(1); //切换默认单选状态
    userInfo.current.setFieldsValue({
      status: 1,
      ...userFrom,
    });
  };
  // 编辑回显
  const editModal = async (record: any) => {
    setTitle('编辑');
    setIsModalOpen(true);
    setItemId(record.userId);
    // 角色信息多选框获取
    const res = await getRoleList();
    setRoleName(res);
    const data = await getUserInfo(record.userId);
    console.log(data);
    setValue(data.status); //切换默认单选状态
    userInfo.current.setFieldsValue({
      password: '',
      pwd: '',
      ...data,
    });
  };
  // 点击确定
  const handleOk = () => {
    setIsModalOpen(false);
  };
  // 点击取消
  const handleCancel = () => {
    setIsModalOpen(false);
    userInfo.current.setFieldsValue({
      status: '',
      ...userFrom,
    });
    // userInfo.current.resetFields();
  };
  // 单选状态
  const changeStatus = (e: RadioChangeEvent) => {
    setValue(e.target.value);
  };
  // 弹框表单提交
  const onFinish = async (values: any) => {
    console.log(values);
    if (title === '编辑') {
      await editUser({
        userId: itemsId,
        ...values,
      });
    } else {
      await addUser({ ...values });
    }
    message.success({
      content: '操作成功',
      style: {
        color: 'rgb(14, 144, 14)',
      },
    });
    userInfo.current.setFieldsValue({
      status: '',
      ...userFrom,
    });
    ref.current.reload();
  };

  // 多选选中项
  const onSelectChange = (allSelectRowkeys: React.Key[]) => {
    // console.log(allSelectRowkeys)
    setSelectRowkeys(allSelectRowkeys);
  };
  const rowSelection = {
    onChange: onSelectChange,
  };
  // 单个/批量删除
  const del = (record: any) => {
    confirm({
      title: '提示',
      icon: <ExclamationCircleOutlined />,
      content: `确定对[id=${
        record.userId ? record.userId : selectRowkeys
      }]进行${record.userId ? '[删除]' : '[批量删除]'}操作?`,
      okText: '确定',
      okType: 'primary',
      cancelText: '取消',
      closable: true, //右上角的关闭按钮
      maskClosable: true, //点击蒙层是否允许关闭
      centered: true, //垂直居中展示 Modal
      maskStyle: { background: 'rgba(255,255,255,.5)' }, //遮罩样式
      async onOk() {
        await delUser(record.userId ? [record.userId] : selectRowkeys);
        message.success({
          content: '操作成功',
          style: {
            color: 'rgb(14, 144, 14)',
          },
        });
        ref.current.reload();
      },
      onCancel() {
        // console.log('Cancel');
      },
    });
  };
  // 数据请求
  const request = async (reg: any) => {
    reg.size = reg.pageSize;
    delete reg.pageSize;
    // 表格数据
    const { records, total } = await getUserList({
      ...reg,
    });
    console.log(records);
    return {
      data: records,
      success: true,
      total,
    };
  };
  // 穿梭时回调
  const handleShowChange = (options) => {
    const arr = columns.filter(
      (item) =>
        options.find((val) => val.key === item.dataIndex)?.isShow ||
        item.notColumnShow,
    );
    setColumnsConfig(arr);
  };

  return (
    <>
      <ProTable
        actionRef={ref}
        pagination={{
          pageSize: 10,
        }}
        columns={columnsConfig}
        request={request} // 表格初始 点击查询 点击分页
        rowKey="userId"
        bordered={true}
        rowSelection={rowSelection}
        toolbar={{
          // 标题栏
          title: [
            <Button key="add" type="primary" onClick={addModal}>
              <PlusOutlined />
              <span>新增</span>
            </Button>,
          ],
          subTitle: [
            <Button
              key="del"
              type="primary"
              danger
              onClick={() => del({})}
              disabled={!selectRowkeys.length > 0}
            >
              批量删除
            </Button>,
          ],
          settings: [
            {
              icon: (
                <button className={style.but}>
                  <RedoOutlined />
                </button>
              ),
              tooltip: '刷新',
              onClick: () => {
                // window.location.reload(); //刷新页面
                ref.current.reload();
              },
            },
            {
              icon: (
                <button className={style.but}>
                  <AppstoreOutlined />
                </button>
              ),
              tooltip: '显隐',
              key: 'show',
              onClick: () => {
                setTransferOpen(true);
              },
            },
            {
              icon: (
                <button className={style.but}>
                  <SearchOutlined />
                </button>
              ),
              tooltip: '搜索',
              onClick: () => {
                console.log(columns, columnsConfig);
                setSearch(!search);
              },
            },
          ],
        }}
        // 重置
        onReset={() => {
          // alert("12121")
        }}
        // 搜索
        form={{
          searchText: `搜索`,
          resetText: '清空',
          collapsed: false,
          collapseRender: false,
          span: 6,
        }}
        search={search}
      ></ProTable>
      {/* 调用显隐公共组件 */}
      <ColumnsTransfer
        columns={columnsConfig}
        show={transferOpen}
        onShowChange={handleShowChange}
        modalConfig={{
          title: '多选',
          onCancel: () => {
            setTransferOpen(false);
          },
        }}
      />
      {/* 修改/新增 对话框 */}
      <Modal
        title={title}
        open={isModalOpen}
        footer={null}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        <Form
          name="basic"
          ref={userInfo}
          labelCol={{ span: 4 }}
          autoComplete="off"
          onFinish={onFinish}
          initialValues={{ remember: true }}
        >
          <Form.Item
            label="用户名"
            name="username"
            rules={[{ required: true, message: '用户名不能为空' }]}
          >
            <Input placeholder="登录账号" />
          </Form.Item>

          <Form.Item
            label="密码"
            name="password"
            rules={[{ required: true, message: '密码不能为空' }]}
          >
            <Input.Password placeholder="密码" />
          </Form.Item>
          <Form.Item
            label="确认密码"
            name="pwd"
            rules={[{ required: true, message: '确认密码不能为空' }]}
          >
            <Input.Password placeholder="确认密码" />
          </Form.Item>
          <Form.Item
            label="邮箱"
            name="email"
            rules={[{ required: true, message: '邮箱不能为空' }]}
          >
            <Input placeholder="邮箱" />
          </Form.Item>
          <Form.Item
            label="手机号"
            name="mobile"
            rules={[{ required: true, message: '手机号不能为空' }]}
          >
            <Input placeholder="手机号" />
          </Form.Item>
          <Form.Item label="角色" name="roleIdList">
            <Checkbox.Group style={{ width: '100%' }}>
              {roleName && roleName.length
                ? roleName.map((item) => {
                    return (
                      <Checkbox key={item.roleId} value={item.roleId}>
                        {item.roleName}
                      </Checkbox>
                    );
                  })
                : ''}
            </Checkbox.Group>
          </Form.Item>
          <Form.Item label="状态" name="status">
            <Radio.Group onChange={changeStatus} value={value}>
              <Radio value={0}>禁用</Radio>
              <Radio value={1}>正常</Radio>
            </Radio.Group>
          </Form.Item>
          <Form.Item wrapperCol={{ offset: 15, span: 16 }}>
            <Button onClick={handleCancel}>取消</Button>
            <Button type="primary" htmlType="submit" onClick={handleOk}>
              确定
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};

export default index;
