import React, { useState } from 'react';
import type { ActionType, ProColumns } from '@ant-design/pro-components';
import { ProTable, TableDropdown } from '@ant-design/pro-components';
import { getSyslog } from '@/services/prod';
import {
  EditOutlined,
  DeleteOutlined,
  PlusOutlined,
  RedoOutlined,
  AppstoreOutlined,
  SearchOutlined,
} from '@ant-design/icons';
import classNames from 'classnames';
import styles from '../../prod/style.less';
import ColumnsTransfer from '@/components/columnsTransfer';
interface prodTableColumns {
  title: string;
}

function index() {
  const columns: ProColumns<prodTableColumns>[] = [
    {
      title: '用户名',
      dataIndex: 'username',
      width: '100px',
      align: 'center',
    },
    {
      title: '用户操作',
      dataIndex: 'operation',
      width: '100px',
      align: 'center',
    },
    {
      title: '请求方法',
      dataIndex: 'method',
      width: '100px',
      align: 'center',
    },
    // {
    //   title: '请求参数',
    //   dataIndex: 'params',
    //   // width: '10%',
    //   align:'center'
    // },
    {
      title: '执行时长',
      dataIndex: 'time',
      width: '100px',
      align: 'center',
    },
    {
      title: 'IP地址',
      dataIndex: 'ip',
      width: '100px',
      align: 'center',
    },
    {
      title: '创建时间',
      dataIndex: 'createDate',
      width: '100px',
      align: 'center',
    },
  ];
  const [transferOpen, setTransferOpen] = useState<boolean>(false);
  const [columnsConfig, setColumnsConfig] = useState<any[]>(columns);
  const [flag, setFlag] = useState(true);

  const request = async (arg: any) => {
    arg.size = arg.pageSize;
    delete arg.pageSize;
    const { records, total } = await getSyslog({
      ...arg,
    });

    return {
      data: records,
      success: true,
      total,
    };
  };
  const handleShowChange = (options) => {
    const arr = columns.filter(
      (item) =>
        options.find((val) => val.key === item.dataIndex)?.isShow ||
        item.notColumnShow,
    );
    setColumnsConfig([...arr]);
  };
  return (
    <div>
      <ProTable
        pagination={{
          pageSize: 10,
        }}
        columns={columnsConfig}
        request={request}
        bordered={true}
        rowSelection={{}}

        rowKey="id"
        toolbar={{
          className: classNames(styles.myToolbar),
          settings: [
            {
              icon: <RedoOutlined />,
              tooltip: '刷新',
            },
            {
              icon: <AppstoreOutlined />,
              tooltip: '显隐',
              key: 'show',
              onClick: () => {
                setTransferOpen(true);
              },
            },
           
            {
              icon: <SearchOutlined />,
              tooltip: '搜索',
              onClick: () => {
                setFlag(!flag);
              },
            },
          ],
        }}
        search={flag}
      ></ProTable>
      <ColumnsTransfer
        columns={columns}
        show={transferOpen}
        onShowChange={handleShowChange}
        modalConfig={{
          title: '多选',
          onCancel: () => {
            setTransferOpen(false);
          },
        }}
      />
    </div>
  );
}

export default index;
