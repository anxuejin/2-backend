import { ProTable } from '@ant-design/pro-components';
import type { ProColumns } from '@ant-design/pro-components';
import {
  getRole,
  delRole,
  getMenu,
  getRoleInfo,
  addRole,
  editRole,
} from '@/services/sys';
import { Tree } from 'antd';
import { Button, Modal, Form, Input, message } from 'antd';
import {
  EditOutlined,
  DeleteOutlined,
  PlusOutlined,
  SearchOutlined,
  AppstoreOutlined,
  RedoOutlined,
  ExclamationCircleOutlined,
} from '@ant-design/icons';
import style from './index.less';
import React, { useState, useRef, useEffect } from 'react';
import ColumnsTransfer from '@/components/columnsTransfer';

interface ProdTableColumns {
  title: string;
  dataIndex?: string;
  notColumnShow?: boolean;
}
interface AreaItem {
  menuId: number;
  parentId: number;
  parentName: null;
  name: string;
  url: null | string;
  perms: string;
  type: number;
  icon: null | string;
  orderNum: number;
  list: null;
}
interface TreeItem {
  key: number;
  title: string;
  children: Array<TreeItem>;
}
const { confirm } = Modal;

// 格式化treeData数据
const formatAreaData = (data: AreaItem[], parentId: number): TreeItem[] => {
  return data
    .filter((val) => Number(val.parentId) === parentId)
    .map((item: AreaItem) => {
      return {
        key: item.menuId,
        title: item.name,
        children: formatAreaData(data, item.menuId),
      };
    });
};

const index: React.FC = () => {
  const columns: ProColumns<ProdTableColumns>[] = [
    {
      title: '角色名称',
      dataIndex: 'roleName',
      align: 'center',
    },
    {
      title: '备注',
      dataIndex: 'remark',
      search: false,
      align: 'center',
    },
    {
      title: '创建时间',
      dataIndex: 'createTime',
      search: false,
      align: 'center',
    },
    {
      title: '操作',
      render(text, record, index) {
        return (
          <li>
            <Button type="primary" onClick={() => editModal(record)}>
              <EditOutlined /> <span>编辑</span>
            </Button>
            &emsp;
            <Button type="primary" danger onClick={() => del(record)}>
              <DeleteOutlined /> <span>删除</span>
            </Button>
          </li>
        );
      },
      search: false,
      notColumnShow: true,
      align: 'center',
    },
  ];
  // 批量删除选中项
  const [selectRowkeys, setSelectRowkeys] = useState<React.Key[]>([]);
  // 选中的复选框
  const [checkedKeys, setCheckedKeys] = useState<React.Key[]>();

  // 点击复选框
  const onCheck = (value: React.Key[]) => {
    // console.log('onCheck', value);
    setCheckedKeys(value);
  };

  // 格式化之后的tree的数据
  const [areaList, setAreaList] = useState<TreeItem[]>([]);
  // 反显的授权
  const [menuIdList, setMenuIdList] = useState([]);
  // 编辑每项的userId
  const [itemsId, setItemId] = useState();
  // 表格标题
  const [title, setTitle] = useState('');
  // 高级筛选栏显示隐藏
  const [search, setSearch] = useState<boolean>(true);
  // 标题栏显隐
  const [transferOpen, setTransferOpen] = useState<boolean>(false);
  const [columnsConfig, setColumnsConfig] = useState<any[]>(columns);
  // 对话框显隐
  const [isModalOpen, setIsModalOpen] = useState(false);
  // 反显的数据
  const userInfo = useRef();
  const ref = useRef();
  // 清空后的表单数据
  const userFrom = {
    createTime: '',
    remark: '',
    menuIdList: [],
    roleId: '',
    roleName: '',
  };

  const init = async () => {
    const data = await getMenu();
    setAreaList(formatAreaData(data, 0)); // 设置tree数据
  };
  // 新增
  const addModal = async () => {
    setTitle('新增');
    setIsModalOpen(true);
  };
  // 编辑回显
  const editModal = async (record: any) => {
    setTitle('编辑');
    setIsModalOpen(true);
    const data = await getRoleInfo(record.roleId);
    console.log(data);
    setItemId(data.roleId);
    setMenuIdList(data.menuIdList);
    userInfo.current.setFieldsValue({
      ...data,
    });
  };
  // console.log(menuIdList);
  // 点击确定
  const handleOk = () => {
    setIsModalOpen(false);
  };
  // 点击取消
  const handleCancel = () => {
    setIsModalOpen(false);
    userInfo.current.setFieldsValue({
      ...userFrom,
    });
  };
  // 弹框表单提交
  const onFinish = async (values: any) => {
    values.menuIdList = checkedKeys;
    console.log(values);
    if (title === '编辑') {
      await editRole({
        roleId: itemsId,
        ...values,
      });
    } else {
      await addRole({ ...values });
    }
    message.success({
      content: '操作成功',
      style: {
        color: 'rgb(14, 144, 14)',
      },
    });
    userInfo.current.setFieldsValue({
      ...userFrom,
    });
    ref.current.reload();
  };

  // 多选选中项
  const onSelectChange = (allSelectRowkeys: React.Key[]) => {
    // console.log(allSelectRowkeys)
    setSelectRowkeys(allSelectRowkeys);
  };
  const rowSelection = {
    onChange: onSelectChange,
  };
  // 单个删除
  const del = (record: any) => {
    confirm({
      title: '提示',
      icon: <ExclamationCircleOutlined />,
      content: `确定对[id=${
        record.roleId ? record.roleId : selectRowkeys
      }]进行${record.roleId ? '[删除]' : '[批量删除]'}操作?`,
      okText: '确定',
      okType: 'primary',
      cancelText: '取消',
      closable: true, //右上角的关闭按钮
      maskClosable: true, //点击蒙层是否允许关闭
      centered: true, //垂直居中展示 Modal
      maskStyle: { background: 'rgba(255,255,255,.5)' }, //遮罩样式
      async onOk() {
        await delRole(record.roleId ? [record.roleId] : selectRowkeys);
        message.success({
          content: '操作成功',
          style: {
            color: 'rgb(14, 144, 14)',
          },
        });
        ref.current.reload();
      },
    });
  };

  // 数据请求
  const request = async (reg: any) => {
    reg.size = reg.pageSize;
    delete reg.pageSize;
    // 表格数据
    const { records, total } = await getRole({
      ...reg,
    });
    // console.log(records);
    return {
      data: records,
      success: true,
      total,
    };
  };
  // 穿梭时回调
  const handleShowChange = (options) => {
    const arr = columns.filter(
      (item) =>
        options.find((val) => val.key === item.dataIndex)?.isShow ||
        item.notColumnShow,
    );
    setColumnsConfig(arr);
  };
  // 进入组件
  useEffect(() => {
    init();
  }, []);

  return (
    <>
      <ProTable
        actionRef={ref}
        pagination={{
          pageSize: 10,
        }}
        columns={columnsConfig}
        request={request} // 表格初始 点击查询 点击分页
        rowKey="roleId"
        bordered={true}
        rowSelection={rowSelection}
        toolbar={{
          // 标题栏
          title: [
            <Button key="add" type="primary" onClick={addModal}>
              <PlusOutlined />
              <span>新增</span>
            </Button>,
          ],
          subTitle: [
            <Button
              key="del"
              type="primary"
              danger
              onClick={() => del({})}
              disabled={!selectRowkeys.length > 0}
            >
              批量删除
            </Button>,
          ],
          settings: [
            {
              icon: (
                <button className={style.but}>
                  <RedoOutlined />
                </button>
              ),
              tooltip: '刷新',
              onClick: () => {
                ref.current.reload(); //刷新页面
              },
            },
            {
              icon: (
                <button className={style.but}>
                  <AppstoreOutlined />
                </button>
              ),
              tooltip: '显隐',
              key: 'show',
              onClick: () => {
                setTransferOpen(true);
              },
            },
            {
              icon: (
                <button className={style.but}>
                  <SearchOutlined />
                </button>
              ),
              tooltip: '搜索',
              onClick: () => {
                setSearch(!search);
              },
            },
          ],
        }}
        // 重置
        onReset={() => {
          // alert("12121")
        }}
        // 搜索
        form={{
          searchText: `搜索`,
          resetText: '清空',
        }}
        search={search}
      ></ProTable>
      {/* 调用显隐公共组件 */}
      <ColumnsTransfer
        columns={columnsConfig}
        show={transferOpen}
        onShowChange={handleShowChange}
        modalConfig={{
          title: '多选',
          onCancel: () => {
            setTransferOpen(false);
          },
        }}
      />
      {/* 修改/新增 对话框 */}
      <Modal
        title={title}
        open={isModalOpen}
        footer={null}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        <Form
          name="basic"
          ref={userInfo}
          labelCol={{ span: 4 }}
          autoComplete="off"
          onFinish={onFinish}
          initialValues={{ remember: true }}
        >
          <Form.Item
            label="角色名称"
            name="roleName"
            rules={[{ required: true, message: '角色名称不能为空' }]}
          >
            <Input placeholder="角色名称" />
          </Form.Item>
          <Form.Item label="备注" name="remark">
            <Input placeholder="备注" />
          </Form.Item>
          <Form.Item label="授权">
            <Tree
              checkable
              defaultCheckedKeys={menuIdList} //默认选中复选框的树节点
              onCheck={onCheck} // 点击复选框
              treeData={areaList}
            />
          </Form.Item>
          <Form.Item wrapperCol={{ offset: 15, span: 16 }}>
            <Button onClick={handleCancel}>取消</Button>
            <Button type="primary" htmlType="submit" onClick={handleOk}>
              确定
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};

export default index;
