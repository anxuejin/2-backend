import React, { useState, useRef } from 'react';
import {
  ProTable,
  TableDropdown,
  useSafeState,
} from '@ant-design/pro-components';
import type { ActionType, ProColumns } from '@ant-design/pro-components';
import {
  getNoticeList,
  delNotice,
  addNotice,
  getNotice,
  putNotice,
} from '@/services/shop';
import {
  Tag,
  Button,
  Select,
  Modal,
  Form,
  Input,
  Radio,
  InputNumber,
} from 'antd';
import {
  EditOutlined,
  DeleteOutlined,
  PlusOutlined,
  RedoOutlined,
  AppstoreOutlined,
  SearchOutlined,
} from '@ant-design/icons';
import style from '../css/index.less';
import ColumnsTransfer from '@/components/columnsTransfer';
import { ExclamationCircleOutlined } from '@ant-design/icons';
const { confirm } = Modal;

interface ProdTableColumns {
  title: string;
  status: number;
  isTop: number;
}
enum Status {
  撤销 = 0,
  公布 = 1,
}

enum isTop {
  否 = 0,
  是 = 1,
}

export default function index() {
  const columns: ProColumns<ProdTableColumns>[] = [
    {
      title: '序号',
      valueType: 'index',
      notColumnShow: true,
    },
    {
      title: '公告内容',
      dataIndex: 'title',
      key: 'title',
    },
    {
      title: '状态',
      dataIndex: 'status',
      render(text, record, index) {
        return record.status === 1 ? (
          <Tag color="blue">{Status[record.status]}</Tag>
        ) : (
          <Tag color="red">{Status[record.status]}</Tag>
        );
      },
      renderFormItem(item, open, form) {
        const handleStatusChange = (value: number) => {
          form.setFieldValue('status', value);
        };
        return (
          <Select placeholder={'状态'} onChange={handleStatusChange}>
            <Select.Option value={Status['撤销']}>{Status[0]}</Select.Option>
            <Select.Option value={Status['公布']}>{Status[1]}</Select.Option>
          </Select>
        );
      },
    },
    {
      title: '是否置顶',
      key: 'isTop',
      render(text, record, index) {
        return <Tag color="blue">{isTop[record.isTop]}</Tag>;
      },
      renderFormItem(item, open, form) {
        const handleIsTopChange = (value: number) => {
          form.setFieldValue('status', value);
        };
        return (
          <Select placeholder={'是否置顶'} onChange={handleIsTopChange}>
            <Select.Option value={isTop['否']}>{isTop[0]}</Select.Option>
            <Select.Option value={isTop['是']}>{isTop[1]}</Select.Option>
          </Select>
        );
      },
    },
    {
      title: '操作',
      render(text, record, index) {
        return (
          <div>
            <Button
              type="primary"
              onClick={() => {
                showModal('修改', record);
              }}
            >
              <EditOutlined />
              修改
            </Button>
            &emsp;
            <Button type="primary" danger onClick={() => del(record)}>
              <DeleteOutlined />
              删除
            </Button>
          </div>
        );
      },
      search: false,
      notColumnShow: true,
    },
  ];

  // 标题栏显隐
  const [transferOpen, setTransferOpen] = useState<boolean>(false);
  const [columnsConfig, setColumnsConfig] = useState<any[]>(columns);
  // 对话框显隐
  const [isModalOpen, setIsModalOpen] = useState(false);
  // 反显的数据
  const userInfo = useRef();
  const ref = useRef();

  // 高级筛选栏显示隐藏
  const [search, setSearch] = useState<boolean>(true);
  const [isModal, setIsModal] = useState(false);
  const del = (record: any) => {
    confirm({
      title: '提示',
      icon: <ExclamationCircleOutlined />,
      content: `确定进行[删除]操作?`,
      okText: '确定',
      okType: 'primary',
      cancelText: '取消',
      closable: true, //右上角的关闭按钮
      maskClosable: true, //点击蒙层是否允许关闭
      centered: true, //垂直居中展示 Modal
      maskStyle: { background: 'rgba(255,255,255,.5)' }, //遮罩样式
      async onOk() {
        await delNotice([record.id]);
        ref.current.reload();
      },
      onCancel() {
        // console.log('Cancel');
      },
    });
  };

  const request = async (arg) => {
    arg.size = arg.pageSize;
    delete arg.pageSize;
    const { records, total } = await getNoticeList({
      ...arg,
    });
    return {
      data: records,
      success: true,
      total,
    };
  };
  const handleShowChange = (options) => {
    const arr = columns.filter(
      (item) =>
        options.find((val) => val.key === item.dataIndex)?.isShow ||
        item.notColumnShow,
    );
    setColumnsConfig(arr);
  };

  const [titles, setTitles] = useState('新增');
  const [itemId, setItemId] = useState();
  const showModal = async (text, record?) => {
    console.log(record, 'record');
    if (text === '新增') {
      setTitles(text);
      setIsModal(true);
    } else if (text === '修改') {
      setTitles(text);
      setIsModal(true);
      setItemId(record.id);
      const data = await getNotice(record.id);
      userInfo.current.setFieldsValue({
        ...data,
      });
    }
  };

  const handleOk = () => {
    setIsModal(false);
  };

  const handleCancel = () => {
    setIsModal(false);
  };

  const onFinish = async (values: any) => {
    console.log('Success:', values);
    if (titles === '新增') {
      let obj = { ...values };
      obj.status = values.status ? 'status' : '1';
      obj.isTop = values.isTop ? 'isTop' : '0';
      obj.url = null;
      obj.content = `<p>1111</p>`;
      await addNotice({ ...obj });
      ref.current.reload();
    } else if (titles === '修改') {
      let obj = { ...values };
      obj.id = itemId;
      obj.status = values.status ? values.status : '1';
      obj.isTop = values.isTop ? values.isTop : '0';
      obj.url = null;
      obj.content = `<p>1111</p>`;
      await putNotice({ ...obj });
      ref.current.reload();
    }
  };

  return (
    <div>
      <ProTable
        rowKey="id"
        actionRef={ref}
        bordered={true}
        toolbar={{
          className: 'toolbar1',
          subTitle: [
            <Button
              key="key"
              type="primary"
              icon={<PlusOutlined />}
              onClick={() => {
                showModal('新增');
              }}
            >
              新增
            </Button>,
          ],
          settings: [
            {
              icon: (
                <button className={style.but}>
                  <RedoOutlined />
                </button>
              ),
              tooltip: '刷新',
              onClick: () => {
                // window.location.reload(); //刷新页面
                ref.current.reload();
              },
            },
            {
              icon: (
                <button className={style.but}>
                  <AppstoreOutlined />
                </button>
              ),
              tooltip: '显隐',
              key: 'show',
              onClick: () => {
                setTransferOpen(true);
              },
            },
            {
              icon: (
                <button className={style.but}>
                  <SearchOutlined />
                </button>
              ),
              tooltip: '搜索',
              onClick: () => {
                setSearch(!search);
              },
            },
          ],
        }}
        pagination={{ pageSize: 5 }}
        columns={columnsConfig}
        request={request}
        search={search}
      />
      <ColumnsTransfer
        columns={columnsConfig}
        show={transferOpen}
        onShowChange={handleShowChange}
        modalConfig={{
          title: '多选',
          onCancel: () => {
            setTransferOpen(false);
          },
        }}
      />
      <Modal
        title={titles}
        destroyOnClose={true}
        footer={null}
        open={isModal}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        <Form
          ref={userInfo}
          name="basic"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          initialValues={{ remember: true }}
          onFinish={onFinish}
          autoComplete="off"
        >
          <Form.Item label="公告标题" name="title">
            <Input />
          </Form.Item>

          <Form.Item label="状态" name="status">
            <Radio.Group name="radiogroup" defaultValue={1}>
              <Radio value={1}>公布</Radio>
              <Radio value={0}>撤销</Radio>
            </Radio.Group>
          </Form.Item>

          <Form.Item label="置顶" name="isTop">
            <Radio.Group name="radiogroup" defaultValue={0}>
              <Radio value={1}>是</Radio>
              <Radio value={0}>否</Radio>
            </Radio.Group>
          </Form.Item>

          <Form.Item label="公告内容" name="status"></Form.Item>

          <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
            <Button onClick={handleOk}>取消</Button>

            <Button type="primary" htmlType="submit" onClick={handleOk}>
              确定
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
}
