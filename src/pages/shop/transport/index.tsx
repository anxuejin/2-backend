import React, { useState, useRef } from 'react';
import { ProTable, TableDropdown } from '@ant-design/pro-components';
import type { ActionType, ProColumns } from '@ant-design/pro-components';
import { getTransportList, delTransport, delTransPort } from '@/services/shop';
import { Tag, Button, Select, Modal } from 'antd';
import {
  EditOutlined,
  DeleteOutlined,
  PlusOutlined,
  AppstoreOutlined,
  SearchOutlined,
  RedoOutlined,
} from '@ant-design/icons';
import style from '../css/index.less';
import ColumnsTransfer from '@/components/columnsTransfer';
import { ExclamationCircleOutlined } from '@ant-design/icons';
const { confirm } = Modal;

interface ProdTableColumns {
  title: string;
  status: number;
  isTop: number;
}
enum Status {
  禁用 = 0,
  正常 = 1,
}

enum isTop {
  否 = 0,
  是 = 1,
}

export default function index() {
  const columns: ProColumns<ProdTableColumns>[] = [
    {
      title: '模版名称',
      dataIndex: 'transName',
      key: 'transName',
    },
    {
      title: '操作',
      render(text, record, index) {
        return (
          <div>
            <Button type="primary">
              <EditOutlined />
              修改
            </Button>
            &emsp;
            <Button type="primary" danger onClick={() => del(record)}>
              <DeleteOutlined />
              删除
            </Button>
          </div>
        );
      },
      search: false,
      notColumnShow: true,
    },
  ];

  // 标题栏显隐
  const [transferOpen, setTransferOpen] = useState<boolean>(false);
  const [columnsConfig, setColumnsConfig] = useState<any[]>(columns);
  // 对话框显隐
  const [isModalOpen, setIsModalOpen] = useState(false);
  // 反显的数据
  const userInfo = useRef();
  const ref = useRef();

  // 高级筛选栏显示隐藏
  const [search, setSearch] = useState<boolean>(true);

  const del = (record: any) => {
    confirm({
      title: '提示',
      icon: <ExclamationCircleOutlined />,
      content: `确定进行[删除]操作?`,
      okText: '确定',
      okType: 'primary',
      cancelText: '取消',
      closable: true, //右上角的关闭按钮
      maskClosable: true, //点击蒙层是否允许关闭
      centered: true, //垂直居中展示 Modal
      maskStyle: { background: 'rgba(255,255,255,.5)' }, //遮罩样式
      async onOk() {
        await delTransport([record.transportId]);
        ref.current.reload();
      },
      onCancel() {
        // console.log('Cancel');
      },
    });
  };

  const request = async (arg) => {
    arg.size = arg.pageSize;
    delete arg.pageSize;
    const { records, total } = await getTransportList({
      ...arg,
    });
    return {
      data: records,
      success: true,
      total,
    };
  };
  const handleShowChange = (options) => {
    const arr = columns.filter(
      (item) =>
        options.find((val) => val.key === item.dataIndex)?.isShow ||
        item.notColumnShow,
    );
    setColumnsConfig(arr);
  };
  let aa = [];
  return (
    <div>
      <ProTable
        rowKey="transportId"
        actionRef={ref}
        bordered={true}
        rowSelection={{
          onChange: (ls) => {
            aa = ls;
          },
        }}
        toolbar={{
          className: 'toolbar1',
          subTitle: [
            <Button
              key="key"
              type="primary"
              icon={<PlusOutlined />}
              onClick={() => {
                alert('add');
              }}
            >
              新增
            </Button>,
            <Button
              key="del"
              type="primary"
              danger
              onClick={() => {
                confirm({
                  title: '提示',
                  icon: <ExclamationCircleOutlined />,
                  content: `确定进行[删除]操作?`,
                  okText: '确定',
                  okType: 'primary',
                  cancelText: '取消',
                  closable: true, //右上角的关闭按钮
                  maskClosable: true, //点击蒙层是否允许关闭
                  centered: true, //垂直居中展示 Modal
                  maskStyle: { background: 'rgba(255,255,255,.5)' }, //遮罩样式
                  async onOk() {
                    await delTransPort(aa);
                    ref.current.reload();
                  },
                  onCancel() {
                    // console.log('Cancel');
                  },
                });
              }}
            >
              批量删除
            </Button>,
          ],
          settings: [
            {
              icon: (
                <button className={style.but}>
                  <RedoOutlined />
                </button>
              ),
              tooltip: '刷新',
              onClick: () => {
                // window.location.reload(); //刷新页面
                ref.current.reload();
              },
            },
            {
              icon: (
                <button className={style.but}>
                  <AppstoreOutlined />
                </button>
              ),
              tooltip: '显隐',
              key: 'show',
              onClick: () => {
                setTransferOpen(true);
              },
            },
            {
              icon: (
                <button className={style.but}>
                  <SearchOutlined />
                </button>
              ),
              tooltip: '搜索',
              onClick: () => {
                setSearch(!search);
              },
            },
          ],
        }}
        pagination={{ pageSize: 5 }}
        columns={columnsConfig}
        request={request}
        search={search}
      />
      <ColumnsTransfer
        columns={columnsConfig}
        show={transferOpen}
        onShowChange={handleShowChange}
        modalConfig={{
          title: '多选',
          onCancel: () => {
            setTransferOpen(false);
          },
        }}
      />
    </div>
  );
}
