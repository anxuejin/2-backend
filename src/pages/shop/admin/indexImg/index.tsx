import React, { useState, useRef } from 'react';
import { ProTable, TableDropdown } from '@ant-design/pro-components';
import type { ActionType, ProColumns } from '@ant-design/pro-components';
import { getAdminList, deladmin, delAdmin } from '@/services/shop';
import {
  Tag,
  Button,
  Select,
  Image,
  Modal,
  Form,
  Input,
  InputNumber,
  Radio,
  Upload,
} from 'antd';
import style from '../../css/index.less';
import ColumnsTransfer from '@/components/columnsTransfer';
import type { RcFile, UploadFile, UploadProps } from 'antd/es/upload/interface';
const { confirm } = Modal;
import {
  EditOutlined,
  DeleteOutlined,
  PlusOutlined,
  RedoOutlined,
  AppstoreOutlined,
  SearchOutlined,
  ExclamationCircleOutlined,
  LoadingOutlined,
} from '@ant-design/icons';

interface ProdTableColumns {
  title?: string;
  status?: number;
  isTop?: number;
  dataIndex?: string;
  notColumnShow?: boolean;
}
enum Status {
  禁用 = 0,
  正常 = 1,
}

enum isTop {
  否 = 0,
  是 = 1,
}

export default function index() {
  const columns: ProColumns<ProdTableColumns>[] = [
    {
      title: '轮播图片',
      dataIndex: 'imgUrl',
      render(text, record, index) {
        return (
          <Image
            width={140}
            src={`http://img-test.gz-yami.com/${record?.imgUrl}`}
            fallback="https://ts1.cn.mm.bing.net/th/id/R-C.d5db82cbbec25fbe9bd559cc82d586fd?rik=kKzks2RY8lCSPg&riu=http%3a%2f%2fbpic.588ku.com%2felement_list_pic%2f19%2f04%2f22%2f53b40a019c5a84ac1aca2a85eea44889.jpg&ehk=4NLlZ%2fhJhYpAhmr8kNXCuCXijJTIkVz6QVq0GsKZPbI%3d&risl=&pid=ImgRaw&r=0&sres=1&sresct=1"
          />
        );
      },
      search: false,
    },
    {
      title: '顺序',
      dataIndex: 'seq',
      key: 'seq',
      search: false,
    },
    {
      title: '状态',
      key: 'status',
      render(text, record, index) {
        return record.status === 1
          ? Status[record.status]
          : Status[record.status];
      },
      renderFormItem(item, open, form) {
        const handleStatusChange = (value: number) => {
          form.setFieldValue('status', value);
        };
        return (
          <Select placeholder={'状态'} onChange={handleStatusChange}>
            <Select.Option value={Status['禁用']}>{Status[0]}</Select.Option>
            <Select.Option value={Status['正常']}>{Status[1]}</Select.Option>
          </Select>
        );
      },
    },
    {
      title: '操作',
      render(text, record, index) {
        return (
          <div>
            <Button type="primary">
              <EditOutlined />
              修改
            </Button>
            &emsp;
            <Button type="primary" danger onClick={() => del(record)}>
              <DeleteOutlined />
              删除
            </Button>
          </div>
        );
      },
      search: false,
      notColumnShow: true,
    },
  ];

  // 标题栏显隐
  const [transferOpen, setTransferOpen] = useState<boolean>(false);
  const [columnsConfig, setColumnsConfig] = useState<any[]>(columns);
  // 对话框显隐
  const [isModalOpen, setIsModalOpen] = useState(false);
  // 反显的数据
  const userInfo = useRef();
  const ref = useRef();

  // 高级筛选栏显示隐藏
  const [search, setSearch] = useState<boolean>(true);
  const [isModal, setIsModal] = useState(false);

  const del = (record: any) => {
    confirm({
      title: '提示',
      icon: <ExclamationCircleOutlined />,
      content: `确定进行[删除]操作?`,
      okText: '确定',
      okType: 'primary',
      cancelText: '取消',
      closable: true, //右上角的关闭按钮
      maskClosable: true, //点击蒙层是否允许关闭
      centered: true, //垂直居中展示 Modal
      maskStyle: { background: 'rgba(255,255,255,.5)' }, //遮罩样式
      async onOk() {
        await deladmin([record.imgId]);
        ref.current.reload();
      },
      onCancel() {
        // console.log('Cancel');
      },
    });
  };

  const request = async (arg) => {
    arg.size = arg.pageSize;
    delete arg.pageSize;
    const { records, total } = await getAdminList({
      ...arg,
    });
    return {
      data: records,
      success: true,
      total,
    };
  };
  // 穿梭时回调
  const handleShowChange = (options) => {
    const arr = columns.filter(
      (item) =>
        options.find((val) => val.key === item.dataIndex)?.isShow ||
        item.notColumnShow,
    );
    setColumnsConfig(arr);
  };
  let aa = [];

  const [titles, setTitles] = useState('新增');
  const [itemId, setItemId] = useState();
  const [imageUrl, setImageUrl] = useState<string>();
  const [loading, setLoading] = useState(false);
  const showModal = async (text, record?) => {
    console.log(record, 'record');
    if (text === '新增') {
      setTitles(text);
      setIsModal(true);
    } else if (text === '修改') {
      setTitles(text);
      setIsModal(true);
      setItemId(record.hotSearchId);
      const data = await getProdTagData(record.hotSearchId);
      userInfo.current.setFieldsValue({
        ...data,
      });
    }
  };

  const handleOk = () => {
    setIsModal(false);
  };

  const handleCancel = () => {
    setIsModal(false);
  };

  const onFinish = async (values: any) => {
    console.log('Success:', values);
    if (titles === '新增') {
      let obj = { ...values };
      obj.hotSearchId = itemId;
      obj.recDate = '';
      // await addHotSearch({ ...obj });
      ref.current.reload();
    } else if (titles === '修改') {
      let obj = { ...values };
      obj.hotSearchId = itemId;
      obj.recDate = '';
      // await putHotSearch({ ...obj });
      ref.current.reload();
    }
  };
  const uploadButton = (
    <div>
      {loading ? <LoadingOutlined /> : <PlusOutlined />}
      <div style={{ marginTop: 8 }}>Upload</div>
    </div>
  );
  return (
    <div>
      <ProTable
        rowKey="imgId"
        bordered={true}
        actionRef={ref}
        rowSelection={{
          onChange: (ls) => {
            aa = ls;
          },
        }}
        toolbar={{
          className: 'toolbar1',
          subTitle: [
            <Button
              key="key"
              type="primary"
              icon={<PlusOutlined />}
              onClick={() => {
                showModal('新增');
              }}
            >
              新增
            </Button>,
            <Button
              key="del"
              type="primary"
              danger
              onClick={() => {
                confirm({
                  title: '提示',
                  icon: <ExclamationCircleOutlined />,
                  content: `确定进行[删除]操作?`,
                  okText: '确定',
                  okType: 'primary',
                  cancelText: '取消',
                  closable: true, //右上角的关闭按钮
                  maskClosable: true, //点击蒙层是否允许关闭
                  centered: true, //垂直居中展示 Modal
                  maskStyle: { background: 'rgba(255,255,255,.5)' }, //遮罩样式
                  async onOk() {
                    await delAdmin(aa);
                    ref.current.reload();
                  },
                  onCancel() {
                    // console.log('Cancel');
                  },
                });
              }}
            >
              批量删除
            </Button>,
          ],
          settings: [
            {
              icon: (
                <button className={style.but}>
                  <RedoOutlined />
                </button>
              ),
              tooltip: '刷新',
              onClick: () => {
                // window.location.reload(); //刷新页面
                ref.current.reload();
              },
            },
            {
              icon: (
                <button className={style.but}>
                  <AppstoreOutlined />
                </button>
              ),
              tooltip: '显隐',
              key: 'show',
              onClick: () => {
                setTransferOpen(true);
              },
            },
            {
              icon: (
                <button className={style.but}>
                  <SearchOutlined />
                </button>
              ),
              tooltip: '搜索',
              onClick: () => {
                setSearch(!search);
              },
            },
          ],
        }}
        pagination={{ pageSize: 5 }}
        columns={columnsConfig}
        request={request}
        search={search}
      />
      <ColumnsTransfer
        columns={columnsConfig}
        show={transferOpen}
        onShowChange={handleShowChange}
        modalConfig={{
          title: '多选',
          onCancel: () => {
            setTransferOpen(false);
          },
        }}
      />
      <Modal
        title={titles}
        destroyOnClose={true}
        footer={null}
        open={isModal}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        <Form
          ref={userInfo}
          name="basic"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          initialValues={{ remember: true }}
          onFinish={onFinish}
          autoComplete="off"
        >
          <Form.Item label="轮播图片" name="title">
            <Upload
              name="avatar"
              listType="picture-card"
              className="avatar-uploader"
              showUploadList={false}
              action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
            >
              {imageUrl ? (
                <img src={imageUrl} alt="avatar" style={{ width: '100%' }} />
              ) : (
                uploadButton
              )}
            </Upload>
          </Form.Item>

          <Form.Item
            label="顺序"
            name="content"
            rules={[{ required: true, message: '不能为空' }]}
          >
            <Input />
          </Form.Item>

          <Form.Item label="状态" name="status">
            <Radio.Group name="radiogroup" defaultValue={0}>
              <Radio value={0}>下线</Radio>
              <Radio value={1}>正常</Radio>
            </Radio.Group>
          </Form.Item>

          <Form.Item label="类型" name="status">
            <Radio.Group name="radiogroup" defaultValue={0}>
              <Radio value={0}>无</Radio>
              <Radio value={1}>商品</Radio>
            </Radio.Group>
          </Form.Item>

          <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
            <Button onClick={handleOk}>取消</Button>

            <Button type="primary" htmlType="submit" onClick={handleOk}>
              确定
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
}
