import React, { useState, useRef } from 'react';
import { ProTable, TableDropdown } from '@ant-design/pro-components';
import type { ActionType, ProColumns } from '@ant-design/pro-components';
import { getPickaddrtList, delPickaddr, delPickAddr } from '@/services/shop';
import {
  Tag,
  Button,
  Select,
  Image,
  Modal,
  Form,
  Input,
  InputNumber,
  Radio,
} from 'antd';
import {
  EditOutlined,
  DeleteOutlined,
  PlusOutlined,
  AppstoreOutlined,
  RedoOutlined,
  SearchOutlined,
} from '@ant-design/icons';
import style from '../css/index.less';
import ColumnsTransfer from '@/components/columnsTransfer';
import { ExclamationCircleOutlined } from '@ant-design/icons';
const { confirm } = Modal;
interface ProdTableColumns {
  title: string;
  status: number;
  isTop: number;
}
enum Status {
  禁用 = 0,
  正常 = 1,
}

enum isTop {
  否 = 0,
  是 = 1,
}

const { Option } = Select;
const provinceData = ['Zhejiang', 'Jiangsu'];
const cityData = {
  Zhejiang: ['Hangzhou', 'Ningbo', 'Wenzhou'],
  Jiangsu: ['Nanjing', 'Suzhou', 'Zhenjiang'],
};

type CityName = keyof typeof cityData;

export default function index() {
  const columns: ProColumns<ProdTableColumns>[] = [
    {
      title: '自提点名称',
      dataIndex: 'addrName',
      key: 'addrName',
    },
    {
      title: '手机号',
      dataIndex: 'mobile',
      key: 'mobile',
      search: false,
    },
    {
      title: '省份',
      dataIndex: 'province',
      key: 'province',
      search: false,
    },
    {
      title: '城市',
      dataIndex: 'city',
      key: 'city',
      search: false,
    },
    {
      title: '区/县',
      dataIndex: 'area',
      key: 'area',
      search: false,
    },
    {
      title: '地址',
      dataIndex: 'addr',
      key: 'addr',
      search: false,
    },
    {
      title: '操作',
      render(text, record, index) {
        return (
          <div>
            <Button type="primary">
              <EditOutlined />
              修改
            </Button>
            &emsp;
            <Button type="primary" danger onClick={() => del(record)}>
              <DeleteOutlined />
              删除
            </Button>
          </div>
        );
      },
      search: false,
      notColumnShow: true,
    },
  ];

  // 标题栏显隐
  const [transferOpen, setTransferOpen] = useState<boolean>(false);
  const [columnsConfig, setColumnsConfig] = useState<any[]>(columns);
  // 对话框显隐
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isModal, setIsModal] = useState(false);
  // 反显的数据
  const userInfo = useRef();
  const ref = useRef();

  // 高级筛选栏显示隐藏
  const [search, setSearch] = useState<boolean>(true);

  const del = (record: any) => {
    confirm({
      title: '提示',
      icon: <ExclamationCircleOutlined />,
      content: `确定进行[删除]操作?`,
      okText: '确定',
      okType: 'primary',
      cancelText: '取消',
      closable: true, //右上角的关闭按钮
      maskClosable: true, //点击蒙层是否允许关闭
      centered: true, //垂直居中展示 Modal
      maskStyle: { background: 'rgba(255,255,255,.5)' }, //遮罩样式
      async onOk() {
        await delPickaddr([record.addrId]);
        ref.current.reload();
      },
      onCancel() {
        // console.log('Cancel');
      },
    });
  };

  const request = async (arg) => {
    arg.size = arg.pageSize;
    delete arg.pageSize;
    const { records, total } = await getPickaddrtList({
      ...arg,
    });
    return {
      data: records,
      success: true,
      total,
    };
  };
  const handleShowChange = (options) => {
    const arr = columns.filter(
      (item) =>
        options.find((val) => val.key === item.dataIndex)?.isShow ||
        item.notColumnShow,
    );
    setColumnsConfig(arr);
  };
  let aa = [];
  const [titles, setTitles] = useState('新增');
  const [itemId, setItemId] = useState();
  const showModal = async (text, record?) => {
    console.log(record, 'record');
    if (text === '新增') {
      setTitles(text);
      setIsModal(true);
    } else if (text === '修改') {
      setTitles(text);
      setIsModal(true);
      setItemId(record.hotSearchId);
      const data = await getProdTagData(record.hotSearchId);
      userInfo.current.setFieldsValue({
        ...data,
      });
    }
  };

  const handleOk = () => {
    setIsModal(false);
  };

  const handleCancel = () => {
    setIsModal(false);
  };

  const onFinish = async (values: any) => {
    console.log('Success:', values);
    if (titles === '新增') {
      let obj = { ...values };
      obj.hotSearchId = itemId;
      obj.recDate = '';
      // await addHotSearch({ ...obj });
      ref.current.reload();
    } else if (titles === '修改') {
      let obj = { ...values };
      obj.hotSearchId = itemId;
      obj.recDate = '';
      // await putHotSearch({ ...obj });
      ref.current.reload();
    }
  };
  const [cities, setCities] = useState(cityData[provinceData[0] as CityName]);
  const [secondCity, setSecondCity] = useState(
    cityData[provinceData[0] as CityName][0],
  );

  const handleProvinceChange = (value: CityName) => {
    setCities(cityData[value]);
    setSecondCity(cityData[value][0]);
  };

  const onSecondCityChange = (value: CityName) => {
    setSecondCity(value);
  };
  return (
    <div>
      <ProTable
        rowKey="addrId"
        actionRef={ref}
        bordered={true}
        rowSelection={{
          onChange: (ls) => {
            aa = ls;
          },
        }}
        toolbar={{
          className: 'toolbar1',
          subTitle: [
            <Button
              key="key"
              type="primary"
              icon={<PlusOutlined />}
              onClick={() => {
                showModal('新增');
              }}
            >
              新增
            </Button>,
            <Button
              key="del"
              type="primary"
              danger
              onClick={() => {
                confirm({
                  title: '提示',
                  icon: <ExclamationCircleOutlined />,
                  content: `确定进行[删除]操作?`,
                  okText: '确定',
                  okType: 'primary',
                  cancelText: '取消',
                  closable: true, //右上角的关闭按钮
                  maskClosable: true, //点击蒙层是否允许关闭
                  centered: true, //垂直居中展示 Modal
                  maskStyle: { background: 'rgba(255,255,255,.5)' }, //遮罩样式
                  async onOk() {
                    await delPickAddr(aa);
                    ref.current.reload();
                  },
                  onCancel() {
                    // console.log('Cancel');
                  },
                });
              }}
            >
              批量删除
            </Button>,
          ],
          settings: [
            {
              icon: (
                <button className={style.but}>
                  <RedoOutlined />
                </button>
              ),
              tooltip: '刷新',
              onClick: () => {
                // window.location.reload(); //刷新页面
                ref.current.reload();
              },
            },
            {
              icon: (
                <button className={style.but}>
                  <AppstoreOutlined />
                </button>
              ),
              tooltip: '显隐',
              key: 'show',
              onClick: () => {
                setTransferOpen(true);
              },
            },
            {
              icon: (
                <button className={style.but}>
                  <SearchOutlined />
                </button>
              ),
              tooltip: '搜索',
              onClick: () => {
                setSearch(!search);
              },
            },
          ],
        }}
        pagination={{ pageSize: 5 }}
        columns={columnsConfig}
        request={request}
        search={search}
      />
      <ColumnsTransfer
        columns={columnsConfig}
        show={transferOpen}
        onShowChange={handleShowChange}
        modalConfig={{
          title: '多选',
          onCancel: () => {
            setTransferOpen(false);
          },
        }}
      />
      <Modal
        title={titles}
        destroyOnClose={true}
        footer={null}
        open={isModal}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        <Form
          ref={userInfo}
          name="basic"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          initialValues={{ remember: true }}
          onFinish={onFinish}
          autoComplete="off"
        >
          <Form.Item
            label="名称"
            name="title"
            rules={[{ required: true, message: '不能为空' }]}
          >
            <Input />
          </Form.Item>

          <Form.Item label="省份" name="content">
            <Select
              defaultValue={provinceData[0]}
              style={{ width: 120 }}
              onChange={handleProvinceChange}
            >
              {provinceData.map((province) => (
                <Option key={province}>{province}</Option>
              ))}
            </Select>
            <Select
              style={{ width: 120 }}
              value={secondCity}
              onChange={onSecondCityChange}
            >
              {cities.map((city) => (
                <Option key={city}>{city}</Option>
              ))}
            </Select>
          </Form.Item>

          <Form.Item
            label="地址"
            name="seq"
            rules={[{ required: true, message: '不能为空' }]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="手机号"
            name="status"
            rules={[{ required: true, message: '不能为空' }]}
          >
            <Input />
          </Form.Item>

          <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
            <Button onClick={handleOk}>取消</Button>

            <Button type="primary" htmlType="submit" onClick={handleOk}>
              确定
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
}
