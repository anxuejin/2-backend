import { defineConfig } from 'umi';
import routes from './routes';
export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  routes,
  layout: {
    // 支持任何不需要 dom 的
    // https://procomponents.ant.design/components/layout#prolayout
    name: '八维后台管理系统',
    locale: true,
    layout: 'side',
    logo:
      'https://img5.tianyancha.com/logo/lll/ae0a32f03df9dd999db480baef125c18.png@!f_200x200',
  },
  fastRefresh: {},
  proxy: {
    '/api': {
      target: 'https://bjwz.bwie.com/mall4w',
      changeOrigin: true,
      pathRewrite: { '^/api': '' },
    },
  },
});
